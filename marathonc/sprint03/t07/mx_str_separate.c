void mx_printchar(char c);

void mx_str_separate(const char *str, char delim) { 
    int flag = 0;
    while (*str != '\0') {
        if (*str == delim && flag == 0) {
            mx_printchar('\n');
            flag = 1;
        }
        if (*str != delim)
            mx_printchar(*str);
        str++;
    }
    mx_printchar('\n');
}
