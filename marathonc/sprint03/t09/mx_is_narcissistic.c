#include <stdbool.h>
double mx_pow(double n, unsigned int pow);
bool mx_is_narcissistic(int num) {
    if (num < 0)
        return false;
    int copy = num;
    int copy2 = num;
    int res = 0;
    int length = 0;
    do {
        copy /= 10;
        length++;
    } while (copy != 0);

    for (int i = 0; i < length; i++) {
        res += mx_pow(num % 10, length);
        num /= 10;
    }
    if (res == copy2)
        return true;
    else
        return false;
}
