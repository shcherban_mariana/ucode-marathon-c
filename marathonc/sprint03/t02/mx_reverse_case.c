#include <stdbool.h>

bool mx_islower(int c);
bool mx_isupper(int c);
int mx_tolower(int c);
int mx_toupper(int c);

void mx_reverse_case(char *s) {
    int index = 0;
    while (s[index] != '\0') {
        if (mx_islower(s[index]))
            s[index] = mx_toupper(s[index]);
        else if (mx_isupper(s[index]))
            s[index] = mx_tolower(s[index]);
        if (s[index] != '\0')
            index++;
    }
}
