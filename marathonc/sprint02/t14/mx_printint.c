void mx_printchar(char c);
void mx_printint(int n) {
    int num = n, count = 0, a = 1;
    for (; num > 0; count++) {
        num /= 10;
        a *= 10;
    }
    a /= 10;
    for (int i = 0; i < count; i++) { 
            mx_printchar(n / a % 10 + '0');
        a /= 10;
    }
}
