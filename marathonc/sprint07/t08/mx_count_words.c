#include <stdbool.h>

int mx_count_words(const char *str, char delimiter) {
    int flag = false;
    int count = 0;
    while (*str) {
        if (*str == delimiter)
            flag = false;
        else if (flag == false) {
            flag = true;
            count++;
        }
        str++;
    }
    return count;
}
