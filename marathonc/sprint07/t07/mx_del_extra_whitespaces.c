#include <stdlib.h>
#include <stdbool.h>

bool mx_isspace(char c);
void mx_strdel(char **str);
char *mx_strnew(const int size);
char *mx_strncpy(char *dst, const char *src, int len);
char *mx_strtrim(const char *str);

char *mx_del_extra_whitespaces(const char *str) {
    char *arr = NULL;
    char *temp = NULL;
    int i = 0;
    int j = 0;

    if (!str)
        return NULL;
    else {
        arr = mx_strnew(mx_strlen(str));
        while (str[i]) {
            if (!(mx_isspace(str[i]))) {
                arr[j] = str[i];
                j++;
            }
            if (!(mx_isspace(str[i])) && mx_isspace(str[i + 1])) {
                arr[j] = ' ';
                j++;
            }
            i++;
        }
        temp = mx_strtrim(arr);
        mx_strdel(&arr);
        return temp;
    }
}
