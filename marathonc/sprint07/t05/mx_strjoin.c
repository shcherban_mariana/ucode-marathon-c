#include <stdlib.h>
#include <stdio.h>
char *mx_strcat(char *s1, const char *s2);
char *mx_strnew(const int size);
char *mx_strdup(const char *str);
int mx_strlen(const char *s);

char *mx_strjoin(const char *s1, const char *s2) {
    if (s1 == NULL && s2 == NULL)
        return NULL;
    else if (s1 == NULL)
        return mx_strdup(s2);
    else if (s2 == NULL)
        return mx_strdup(s1);
    else {
        int size_s1 = mx_strlen(s1);
        int size_s2 = mx_strlen(s2);
        char *new_copy = mx_strnew(size_s1 + size_s2);
        new_copy = mx_strcat(new_copy, s1);
        new_copy = mx_strcat(new_copy, s2);
        return new_copy;
    }
}
