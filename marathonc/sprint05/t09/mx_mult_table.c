#include <stdbool.h>

int mx_atoi(const char *str);
bool mx_isspace(char c);
bool mx_isdigit(int c);
void mx_printchar(char c);
void mx_printint(int n);
int mx_strlen(const char *s);

int main(int argc, char *argv[]) {

    if (argc != 3) {
        return 0;
    }
    if (!mx_isdigit(*argv[1]) || !mx_isdigit(*argv[2])) {
        return 0;
    }

    int x = mx_atoi(argv[1]);
    int y = mx_atoi(argv[2]);

    if (x < 0 || y < 0 || x > 9 || y > 9) {
        return 0;
    }

    if (x > y) {
        int temp = x;
        x = y;
        y = temp;
    }

    for (int i = x; i <= y; i++) {
        for (int j = x; j <= y; j++) {
            mx_printint(i * j);
            if (j != y) {
                mx_printchar('\t');
            }
        }
        mx_printchar('\n');
    }
    return 0;
}
