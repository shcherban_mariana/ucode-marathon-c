#include <stdbool.h>

int mx_atoi(const char *str);
void mx_printchar(char c);
void mx_printint(int n);
bool mx_isspace(char c);
bool mx_isdigit(int c);

int main(int argc, char *argv[]) {
    int sum = 0;
    if (argc < 2) {
        return 0;
    }

    for (int i = 1; i < argc; i++) {
        int num = 0;
        int minus = 0;
        int transformed = 1;

        for (int j = 0; argv[i][j] != '\0'; j++) {
            if (j == 0 && argv[i][j] == '-') {
                minus = 1;
                continue;
            }
            if (j == 0 && argv[i][j] == '+') {
                minus = 0;
                continue;
            }
            if (mx_isdigit(argv[i][j]) == false) {
                transformed = 0;
                break;
            }
            num = 10 * num + (int)argv[i][j] - 48;
        }

        if (transformed == 1) {
            if (minus == 0) {
                sum += num;
            }
            else {
                sum -= num;
            }
        }
    }
    mx_printint(sum);
    mx_printchar('\n');
    return 0;
}
