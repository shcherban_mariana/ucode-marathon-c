void mx_printchar(char c);
void mx_printstr(const char *s);
int mx_strcmp(const char *s1, const char *s2);
int mx_strlen(const char *s);

int main(int argc, char *argv[]) {
    if (argc > 1) {
        for (int x = 0; x < argc; x++) {
            for (int y = x + 1; y < argc; y++) {
                if ((mx_strcmp(argv[x], argv[y])) > 0) {
                    char *temp = argv[x];
                    argv[x] = argv[y];
                    argv[y] = temp;
                }
            }
        }
        for (int i = 1; i < argc; i++) {
            mx_printstr(argv[i]);
            mx_printchar('\n');
        }
    }
    return 0;
}
