#include "duplicate.h"

t_intarr *mx_del_dup_sarr(t_intarr *src) {
    if (src->arr == NULL)
        return NULL;
    int k = 0;
    int flag = 0;
    bool dst;
    t_intarr *res_arr = (t_intarr *)malloc(16);

    for (int i = 0; i < src->size; i++) {
        for (int j = 0; j < i; j++) {
            if (src->arr[j] == src->arr[i]) {
                flag++;
                break;
            }
        }
    }
    res_arr->size = src->size - flag;
    res_arr->arr = mx_copy_int_arr(src->arr, res_arr->size);
    for (int i = 0; i < src->size; i++) {
        dst = false;
        for (int j = 0; j < i; j++) {
            if (src->arr[j] == src->arr[i] && k < res_arr->size) {
                dst = true;
                break;
            }
        }
        if (!dst)
            res_arr->arr[k++] = src->arr[i];
    }
    return res_arr;
}
