#include <stdlib.h>

static char *mx_strnew(const int size) {
    char *arr = NULL;
    int i = 0;

    if (size < 0)
        return NULL;
    arr = (char *)malloc((size + 1));
    while (i < size) {
        arr[i] = '\0';
        i++;
    }
    arr[i] = '\0';
    return arr;
}

char *mx_nbr_to_hex(unsigned long nbr) {
    if (nbr < 1)
        return 0;
    unsigned long size = 0, nbr_copy = nbr;
    while (nbr_copy) {
        unsigned long num = (nbr_copy % 16);
        if (num > 9)
            num += (unsigned long)'a' - 10;
        else
            num += (unsigned long)'0';
        size++;
        nbr_copy /= 16;
    }

    char *temp = mx_strnew(size);

    while (nbr) {
        unsigned long num = (nbr % 16);
        if (num > 9)
            num += (unsigned long)'a' - 10;
        else
            num += (unsigned long)'0';
        temp[--size] = (char)num;
        nbr /= 16;
    }
    return temp;
}
