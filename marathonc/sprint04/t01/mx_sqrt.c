int mx_sqrt(int x) {
    int i = 1;
    if(x > 0) {
        while((i < x / 2) || x == 1) {
            if(i * i == x)
                return i;
            i++;
        }
        return 0;
    }
    return 0;
}
