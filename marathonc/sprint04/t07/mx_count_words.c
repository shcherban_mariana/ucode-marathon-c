int mx_count_words(const char *str, char delimiter) {
    int flag = 0;
    int res = 0;
    while (*str) {

        if (*str == delimiter || *str == '\n')
            flag = 0;

        else if (flag == 0) {
            flag = 1;
            ++res;
        }
        ++str;
    }
    return res;
}
