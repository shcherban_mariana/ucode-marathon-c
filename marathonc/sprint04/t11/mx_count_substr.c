char *mx_strstr(const char *s1, const char *s2);
int mx_strlen(const char *s);
int mx_strncmp(const char *s1, const char *s2, int n);
char *mx_strchr(const char *s, int c);

int mx_count_substr(const char *str, const char *sub) {
    int sb = mx_strlen(sub);
    int st = mx_strlen(str);
    int result = 0;

    for (int i = 0; i <= st - sb; i++) {
        int j;
        for (j = 0; j < sb; j++)
            if (str[i + j] != sub[j])
                break;

        if (j == sb) {
            result++;
            j = 0;
        }
    }
    return result;
}
