int mx_popular_int(const int *arr, int size) {
    int result = arr[0], maxCount = 1, currentCount = 1;
    for (int i = 1; i < size; i++) {
        if (arr[i] == arr[i - 1])
            currentCount++;
        else {
            if (currentCount > maxCount) {
                maxCount = currentCount;
                result = arr[i - 1];
            }
            currentCount = 1;
        }
    }
    if (currentCount > maxCount) {
        maxCount = currentCount;
        result = arr[size - 1];
    }
    return result;
}
