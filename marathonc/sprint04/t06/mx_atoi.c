#include <stdbool.h>
bool mx_isspace(int c);
bool mx_isdigit(int c);

int mx_atoi(const char *str) {
    int result = 0;
    int sign = 1;
    for (int i = 0; str[i] != '\0'; i++) {
        if (mx_isspace(str[i]) || str[i] == '0')
            continue;
        else if (str[i] == '-' && !mx_isdigit(str[i + 1]))
            return 0;
        else if (str[i] == '-' && mx_isdigit(str[i + 1]))
            sign = -1;
        else if (mx_isdigit(str[i])) {
            result = result * 10 + str[i] - '0';
            if (!mx_isdigit(str[i + 1]))
                break;
        }
    }
    return result * sign;
}
