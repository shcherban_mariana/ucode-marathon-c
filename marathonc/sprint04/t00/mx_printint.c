void mx_printchar(char c);
void mx_printint(int n) {
    if (n < 0) {
        mx_printchar('-');
        n *= -1;
    }
    int count = 0;
    while (n) {
        count = count * 10 + n % 10;
        n /= 10;
    }
    while (count) {
        mx_printchar((count % 10) + 48);
        count /= 10;
    }
}
