#include <unistd.h>

void mx_printint(int n) {
    if (n != 0) {
        char c[20];
        int i = 0;
        long count = n;

        if (count < 0) {
            write(1, "-", 1);
            count = -count;
        }

        while (count != 0) {
            c[i++] = count % 10 + '0';
            count /= 10;
        }

        for (int j = i - 1; j >= 0; j--)
            write(1, &c[j], 1);
    }
    else
        write(1, "0", 1);
}
