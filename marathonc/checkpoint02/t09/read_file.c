#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

char *mx_strnew(const int size);
void mx_printchar(char c);
void mx_printerr(const char *s);
int mx_strlen(const char *s);
char *mx_strnew(const int size);
void mx_printstr(const char *s);

void mx_printchar(char c) {
    write(1, &c, 1);
}

void mx_printstr(const char *s) {
    write(1, s, mx_strlen(s));
}

void mx_printerr(const char *s) {
    write(2, s, mx_strlen(s));
}

int mx_strlen(const char *s) {
    int count = 0;
    while (s[count])
        count++;
    return count;
}

char *mx_strnew(const int size) {
    char *buff = NULL;
    if (size > 0)
    {
        buff = malloc(size + 1);
        buff[size] = '\0';
    }
    return buff;
}

int main(int argc, char *argv[]) {
    int dist;
    int len = 0;
    int i = 0;
    char buf[1];
    char *str = NULL;
    const char *s = "error";
    const char *v = "usage: ./read_file [file_path]";

    if (argc == 2) {
        dist = open(argv[1], O_RDONLY);
        if (dist < 0) {
            mx_printerr(s);
            return 0;
        }
        while (read(dist, buf, 1))
            len++;
        str = (char *)mx_strnew(len);
        if (str == NULL) {
            mx_printerr(s);
            return 0;
        }
        close(2);
        dist = open(argv[1], O_RDONLY);
        if (dist < 0) {
            mx_printerr(s);
            return 0;
        }
        while (read(dist, buf, 1)) {
            str[i] = buf[0];
            i++;
        }
        str[i] = '\0';
        mx_printstr(str);
    }
    else {
        mx_printstr(v);
        mx_printchar('\n');
    }
}
