#include <stdlib.h>

static char *mx_strnew(const int size) {
    char *arr = NULL;
    int i = 0;

    if (size < 0)
        return NULL;
    arr = (char *)malloc((size + 1));
    while (i < size) {
        arr[i] = '\0';
        i++;
    }
    arr[i] = '\0';
    return arr;
}

static char *mx_strcpy(char *dst, const char *src) {
    int i = 0;

    while (src[i]) {
        dst[i] = src[i];
        i++;
    }
    dst[i] = '\0';
    return dst;
}

char *mx_strdup(const char *s1) {
    int c = 0;
    char i = s1[0];
    while (i != '\0') {
        c++;
        i = s1[c];
    }
    char *duplicate = mx_strnew(c);

    mx_strcpy(duplicate, s1);
    return duplicate;
}
