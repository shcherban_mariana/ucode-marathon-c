int mx_strcmp(const char *s1, const char *s2);
int mx_strlen(const char *s);
int mx_selection_sort(char **arr, int size) {
    char *temp = 0;
    int min_idx = 0;
    int flag = 0;
    int count = 0;
    for (int i = 0; i < size - 1; i++) {
        min_idx = i;
        for (int j = i + 1; j < size; j++) {
            if (mx_strcmp(arr[min_idx], arr[j]) > 0) {
                arr[min_idx] = arr[j];
                min_idx = j;
                flag++;
            }
        }
        if (flag > 0) {
            temp = arr[min_idx];
            arr[min_idx] = arr[i];
            arr[i] = temp;
            count++;
        }
        flag = 0;
    }
    return count;
}
