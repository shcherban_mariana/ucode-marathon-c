#include <stdbool.h>
 
bool mx_isspace(char c);
bool mx_isdigit(int c);
int mx_atoi(const char *str);

    int mx_atoi(const char *str) {
    int sign = 1;
    int res = 0;
    for (int i = 0; str[i] != '\0'; i++) {
        if (mx_isspace(str[i]) || str[i] == '0')
            continue;
        else if (str[i] == '-' && !mx_isdigit(str[i + 1]))
            return 0;
        else if (str[i] == '-' && mx_isdigit(str[i + 1]))
            sign = -1;
        else if (mx_isdigit(str[i])) {
            res = res * 10 + str[i] - '0';
            if (!mx_isdigit(str[i + 1]))
                break;
        }
    }
    return res * sign;
}
