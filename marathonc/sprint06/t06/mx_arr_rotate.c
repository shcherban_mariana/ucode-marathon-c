void mx_arr_rotate(int *arr, int size, int shift) {
    int temp = 0;
    int x = 0;
    int tempArr[size];
    for (int i = 0; i < size; i++) {
        tempArr[i] = arr[i];
    }
    if (shift > 0) {
        for (int i = 0; i < shift; i++) {
            x = arr[size - 1];
            for (int j = 0; j < size; j++) {
                temp = tempArr[j];
                arr[j + 1] = temp;
            }
            arr[0] = x;
            for (int i = 0; i < size; i++) {
                tempArr[i] = arr[i];
            }
        }
    }
    else if (shift < 0) {
        for (int i = shift; i < 0; i++) {
            x = arr[0];
            for (int j = size - 1; j > 0; j--) {
                temp = tempArr[j];
                arr[j - 1] = temp;
            }
            arr[size - 1] = x;
            for (int i = 0; i < size; i++) {
                tempArr[i] = arr[i];
            }
        }
    }
}
