#include <unistd.h>
void mx_printchar(char c);
void mx_hexadecimal(void) {
    char ch;
    for(int i = 0; i < 10; i++) {
        ch = i + '0';
        mx_printchar(ch);
        write(1, "\n", 1);
    }
    for(int i = 65; i < 71; i++) {
        mx_printchar(i);
        write(1, "\n", 1);
    }
}
