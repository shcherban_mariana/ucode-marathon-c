#include "header.h"

void mx_operations(char *operand1, char *operation, char *operand2, char *result) {
    if (*operation == '?') {
        mx_operations(operand1, "+", operand2, result);
        mx_operations(operand1, "-", operand2, result);
        mx_operations(operand1, "*", operand2, result);
        mx_operations(operand1, "/", operand2, result);
        return;
    }

    int operand1_len = mx_strlen(operand1);
    int operand2_len = mx_strlen(operand2);
    int res_len = mx_strlen(result);
    bool isOperand1Negative = false;
    bool isOperand2Negative = false;
    bool isResultNegative = false;

    if (operand1[0] == '-') {
        isOperand1Negative = true;
        operand1++;
        operand1_len--;
    }

    if (operand2[0] == '-') {
        isOperand2Negative = true;
        operand2++;
        operand2_len--;
    }

    if (result[0] == '-') {
        isResultNegative = true;
        result++;
        res_len--;
    }

    int operand1Num = mx_atoi(operand1);
    int operand2Num = mx_atoi(operand2); 
    int resultNum = mx_atoi(result);

    for (long long i = 0; i < mx_pow(10, operand1_len); i++) {
        if (operand1Num > 0) {
            i = operand1Num;
        }
        char *iDist = mx_itoa(i);
        int ilen = mx_strlen(iDist);
        bool isIcorrect = true;

        for (int j = 0; j < operand1_len - ilen; j++) {
            if (mx_isdigit(operand1[j]) && operand1[j] != '0') {
                isIcorrect = false;
                break;
            }
        }

        for (int j = operand1_len - ilen, k = 0; j < operand1_len; j++, k++) {
            if (operand1[j] != '?' && operand1[j] != iDist[k]) {
                isIcorrect = false;
                break;
            }
        }

        free(iDist);
        iDist = NULL;
        if (!isIcorrect)
            continue;

        for (long long j = 0; j < mx_pow(10, operand2_len); j++) {
            if (operand2Num > 0) {
                j = operand2Num;
            }
            char *jDist = mx_itoa(j);
            int jlen = mx_strlen(jDist);
            bool isJCorrect = true;

            for (int p = 0; p < operand2_len - jlen; p++) {
                if (mx_isdigit(operand2[p]) && operand2[p] != '0') {
                    isJCorrect = false;
                    break;
                }
            }

            for (int p = operand2_len - jlen, jqueue_i = 0; p < operand2_len; p++, jqueue_i++) {
                if (operand2[p] != '?' && operand2[p] != jDist[jqueue_i]) {
                    isJCorrect = false;
                    break;
                }
            }

            free(jDist);
            jDist = NULL;

            if (!isJCorrect)
                continue;

            for (long long k = 0; k < mx_pow(10, res_len); k++) {
                if (resultNum > 0) {
                    k = resultNum;
                }

                char *kDist = mx_itoa(k);
                int kqueue_len = mx_strlen(kDist);
                bool isCorrectK = true;

                for (int n = 0; n < res_len - kqueue_len; n++) {
                    if (mx_isdigit(result[n]) && result[n] != '0') {
                        isCorrectK = false;
                        break;
                    }
                }

                for (int n = res_len - kqueue_len, kqueue_i = 0; n < res_len; n++, kqueue_i++) {
                    if (result[n] != '?' && result[n] != kDist[kqueue_i]) {
                        isCorrectK = false;
                        break;
                    }
                }

                free(kDist);
                kDist = NULL;
                if (!isCorrectK)
                    continue;

                if (isOperand1Negative)
                    i *= -1;
                if (isOperand2Negative)
                    j *= -1;
                if (isResultNegative)
                    k *= -1;

                if (*operation == '+') {
                    if (i + j == k) {
                        mx_printint(i);
                        mx_printstr(" + ");
                        mx_printint(j);
                        mx_printstr(" = ");
                        mx_printint(k);
                        mx_printchar('\n');
                    }
                }
                else if (*operation == '-') {
                    if (i - j == k) {
                        mx_printint(i);
                        mx_printstr(" - ");
                        mx_printint(j);
                        mx_printstr(" = ");
                        mx_printint(k);
                        mx_printchar('\n');
                    }
                }
                else if (*operation == '*') {
                    if (i * j == k) {
                        mx_printint(i);
                        mx_printstr(" * ");
                        mx_printint(j);
                        mx_printstr(" = ");
                        mx_printint(k);
                        mx_printchar('\n');
                    }
                }
                else if (*operation == '/') {
                    if (j != 0 && i / j == k) {
                        mx_printint(i);
                        mx_printstr(" / ");
                        mx_printint(j);
                        mx_printstr(" = ");
                        mx_printint(k);
                        mx_printchar('\n');
                    }
                }

                if (isOperand1Negative)
                    i *= -1;
                if (isOperand2Negative)
                    j *= -1;
                if (isResultNegative)
                    k *= -1;
                if (resultNum > 0)
                    break;
            }
            if (operand2Num > 0)
                break;
        }
        if (operand1Num > 0)
            break;
    }

    if (isOperand1Negative)
        operand1--;
    if (isOperand2Negative)
        operand2--;
    if (isResultNegative)
        result--;
}
