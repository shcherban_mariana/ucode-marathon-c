#include "../inc/uls.h"

static void mx_argv_sort(char **argv, int start, int argc) {
    char *temp_arr;
    for (int i = start; i != argc - 1; i++) {
            for (int j = i + 1; j != argc; j++) {
                if (mx_strcmp(argv[i], argv[j]) > 0) {
                    temp_arr = argv[i];
                    argv[i] = argv[j];
                    argv[j] = temp_arr;
            }
        }
    }
}

int main(int argc, char *argv[]) {
    int count = 1;
    int start = 0;
    
    if (argc == 1)
        ;
    else if(argc > 3 && (mx_strcmp(argv[1],"-l")) == 0) {
        start = 2;
        mx_argv_sort(argv, start, argc);
    }
    else if (mx_strcmp(argv[1],"-l") != 0) {
        start = 1;
        mx_argv_sort(argv, start, argc);
    }

    s_flags *fl = mx_check_flags(argv, &count);
    s_list **args = mx_get_names(argc, argv, count);

    if (args)
        mx_open_dir1(&args, fl);

    free(fl);
    fl = NULL;
}
