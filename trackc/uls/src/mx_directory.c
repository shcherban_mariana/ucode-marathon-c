#include "../inc/uls.h"

int mx_count_read(s_list **arg) {
    int count = 0;
    s_list *args = *arg;
    DIR *dh;
    struct dirent *ds;

    if (IS_DIR(args->info.st_mode) || IS_LNK(args->info.st_mode)) {
        if ((dh = opendir(args->path)) != NULL) {
            while ((ds = readdir(dh)) != NULL)
                if (ds->d_name[0] != '.')
                    count++;
                    
            closedir(dh);
        }
        else {
            (*arg)->error = mx_strdup(strerror(errno));
            return -1;
        }
    }
    return count;
}

s_list *create_li_node1(char *name, char *path) {
    s_list *node = (s_list *)malloc(1 * sizeof(s_list));

    node->name = mx_strdup(name);
    node->path = mx_strdup(path);
    mx_join(&node->path, "/");
    mx_join(&node->path, name);
    node->error = NULL;
    if (lstat(node->path, &(node->info)) == -1)
        node->error = mx_strdup(strerror(errno));
    node->open = NULL;
    return node;
}

void mx_open_dir(s_list ***args, s_flags *fl) {
    DIR *dh;
    struct dirent *ds;
    int count = 0;

    for (int i = 0; (*args)[i] != NULL; i++) {
        count = mx_count_read(&(*args)[i]);
        if (count > 0) {
            (*args)[i]->open = malloc((count + 1) * sizeof(s_list *));
            if ((dh = opendir((*args)[i]->path)) != NULL) {
                for (count = 0; (ds = readdir(dh)) != NULL;)
                    if (ds->d_name[0] != '.')
                        (*args)[i]->open[count++] = 
                        create_li_node1(ds->d_name, (*args)[i]->path);
                (*args)[i]->open[count] = NULL;
            closedir(dh);
            }
        }
    }
    mx_out_general(args, fl);
}

void mx_open_dir1(s_list ***args, s_flags *fl) {
    s_list **file = mx_get_files(&(*args), fl);

	if (file) {
		mx_out_menu(&file, fl, 0);
		if (*args)
			mx_printchar('\n');
        mx_del_arr(&file);
	}
    if (*args)
        mx_open_dir(&(*args), fl);
}
