#include "../inc/uls.h"

void mx_print_permission(s_list *data) {
    char *chmod = malloc(12 * sizeof(char));
    for (int i = 0; i != 12; i++)
        chmod[i] = '-';
    
    if (IS_DIR(data->info.st_mode))
        chmod[0] = 'd';
    else if (IS_LNK(data->info.st_mode))
        chmod[0] = 'l';
    else if (IS_BLK(data->info.st_mode))
        chmod[0] = 'b';
    else if (IS_CHR(data->info.st_mode))
        chmod[0] = 'c';
    else if (IS_FIFO(data->info.st_mode))
        chmod[0] = 'p';
    else if (IS_SOCK(data->info.st_mode))
        chmod[0] = 's';
    else if (IS_WHT(data->info.st_mode))
        chmod[0] = 'w';
    else    
        chmod[0] = '-';

    if (S_IRUSR & data->info.st_mode)
        chmod[1] = 'r';
    if (S_IWUSR & data->info.st_mode)
        chmod[2] = 'w';
    if (S_IXUSR & data->info.st_mode)
        chmod[3] = 'x';
    if (S_IRGRP & data->info.st_mode)
        chmod[4] = 'r';
    if (S_IWGRP & data->info.st_mode)
        chmod[5] = 'w';
    if (S_IXGRP & data->info.st_mode)
        chmod[6] = 'x';
    if (S_IROTH & data->info.st_mode)
        chmod[7] = 'r';
    if (S_IWOTH & data->info.st_mode)
        chmod[8] = 'w';
    if (S_IXOTH & data->info.st_mode)
        chmod[9] = 'x';

    chmod[10] = mx_acl(data);
    chmod[11] = '\0';

    for (int i = 0; chmod[i]; i++)
        mx_printchar(chmod[i]);

    mx_printchar(' ');
    free(chmod);
}

void mx_print_uname(s_list *data, int user) {
    struct passwd *pw = getpwuid(data->info.st_uid);
    int counter = 0;
    char *name = NULL;

    if (pw)
        name = mx_strdup(pw->pw_name);
    else
        name = mx_itoa(data->info.st_uid);
    if (mx_strlen(name) == user)
       mx_printstr(name);
    else if (mx_strlen(name) < user) {
        counter = mx_strlen(name);
        mx_printstr(name);
        while (counter != user) {
            mx_printchar(' ');
            counter++;
        }
    }
    mx_printstr("  ");
    free(name);
}

void mx_print_gname(s_list *data, int group) {
    struct group *grp = getgrgid(data->info.st_gid);
    int counter = 0;
    char *name = NULL;

    if (grp)
        name = mx_strdup(grp->gr_name);
    else
        name = mx_itoa(data->info.st_gid);
    if (mx_strlen(name) == group)
        mx_printstr(name);
    else if (mx_strlen(name) < group) {
        counter = mx_strlen(name);
        mx_printstr(name);
        while (counter != group) {
            mx_printchar(' ');
            counter++;
        }
    }
    mx_printstr("  ");
    free(name);
}

void mx_print_size(s_list *data, s_size *size) {
    char *res_current = mx_itoa(data->info.st_size);
    char *res_size = mx_itoa(size->size);
    int counter = 0;
    
    if (mx_strlen(res_current) == mx_strlen(res_size))
        mx_printint(data->info.st_size); 
    else if (mx_strlen(res_current) < mx_strlen(res_size)) {
        counter = mx_strlen(res_current);
        while (counter != mx_strlen(res_size)) {
            mx_printchar(' ');
            counter++;
        }
        mx_printint(data->info.st_size);
    }
    mx_printchar(' ');
    free(res_current);
    free(res_size);
}


void mx_print_time(s_list *data, char *t) {
    if (1565913600 >= data->info.st_mtime) {
        for(int i = 4; i < 10; i++)
            mx_printchar(t[i]);
        mx_printstr("  ");
        
        for (int i = 20; i < 24; i++)
            mx_printchar(t[i]); 
    }
    else
        for(int i = 4; i < 16; i++)
            mx_printchar(t[i]);
    
    mx_printstr(" ");
}

void mx_print_link(s_list *data, s_size *size) {
    int counter = 0;
    char *res_current = mx_itoa(data->info.st_nlink);
    char *res_link = mx_itoa(size->link);

    if (mx_strlen(res_current) == mx_strlen(res_link)) {
        mx_printint(data->info.st_nlink);
    }
    else if (mx_strlen(res_current) < mx_strlen(res_link)) {
        counter = mx_strlen(res_current);
        while (counter != mx_strlen(res_link)) {
            mx_printchar(' ');
            counter++;
        }
        mx_printint(data->info.st_nlink);
    }
    mx_printchar(' ');

    free(res_current);
    free(res_link);
}

void mx_print_subgeneral(s_list *data, s_size *size) {
    time_t *time = &data->info.st_mtime;

    mx_print_permission(data);
    mx_print_link(data, size);
    mx_print_uname(data, size->user);
    mx_print_gname(data, size->group);
    mx_print_size(data, size);
    mx_print_time(data, ctime(time));
    mx_printstr(data->name);

    mx_printchar('\n');
}

void mx_print_general(s_list **names, int flag) {
    s_size *size = malloc(sizeof(s_size));
    size->link = 0;
    size->size = 0;
    size->group = 0;
    size->user = 0;

    int total_sz = 0;
    int i = 0;

    for (i = 0; names[i]; i++) {
        total_sz += names[i]->info.st_blocks;
        mx_count_size(size, names[i]);
    }
    if (flag == 1) {
        mx_printstr("total ");
        mx_printint(total_sz);
        mx_printchar('\n');
    }
    
    for (i = 0; names[i]; i++)
        mx_print_subgeneral(names[i], size);
    free(size);
}
