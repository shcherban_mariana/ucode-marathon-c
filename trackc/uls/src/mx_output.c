#include "../inc/uls.h"

static void mx_out_cat(s_list **names) {
    for (int i = 0; names[i]; i++) {
        mx_printstr(names[i]->name);
        mx_printchar('\n');
    }
}

void mx_out_menu(s_list ***names, s_flags *fl, int flag) {
    if (**names != NULL) {
        mx_sort(&(*names));
        
        if(fl->l)
            mx_print_general(*names, flag);
        else
            (void) 0;
        if (fl->l != 1 && isatty(1))
            mx_out_column(*names);

        if (!isatty(1) && fl->l != 1)
        mx_out_cat(*names);
    }
}

void mx_output_error(s_list **args, s_flags *fl) {
    if ((*args)->open != NULL) {
        mx_out_menu(&(*args)->open, fl, 1);
    }
    else if ((*args)->error != NULL) {
        mx_printerr("uls: ");
        mx_printerr((*args)->path);
        mx_printerr(": ");
        mx_printerr((*args)->error);
        mx_printerr("\n");
    }
}

void mx_out_general(s_list ***args, s_flags *fl) {
    if (*args) {
        for (int i = 0; (*args)[i] != NULL; i++) {
            if (fl->files == 1) {
                if ((*args)[i]->path[0] == '/' && (*args)[i]->path[1] == '/')
                    mx_printstr(&(*args)[i]->path[1]);
                else
                    mx_printstr((*args)[i]->path);
                mx_printchar(':');
                mx_printchar('\n');
            }
            mx_output_error(&(*args)[i], fl);
            if (fl->files == 1 && (*args)[i + 1])
                mx_printchar('\n');
        }
        mx_del_arr(args);
        }
    }

void mx_out_error(s_list ***error) {
    if (error && *error && **error) {
        mx_sort(error);
        for (int i = 0; (*error)[i]; i++) {
            mx_printerr("uls: ");
            mx_printerr((*error)[i]->name);
            mx_printerr(": ");
            mx_printerr((*error)[i]->error);
            mx_printerr("\n");

            mx_strdel(&(*error)[i]->name);
            mx_strdel(&(*error)[i]->path);
            mx_strdel(&(*error)[i]->error);
            free((*error)[i]);
            (*error)[i] = NULL;
        }

        free(*error);
        *error = NULL;
    }
}


// column output
int mx_len_names(s_list **names) {
    int max_len = 0;
    int temp = 0;

    for (int i = 0; names[i]; i++) {
        temp = mx_strlen(names[i]->name);
        if (temp > max_len)
            max_len = temp;
    }

    //max_len = (max_len % 8 == 0) ? (max_len += 8) : (max_len += 8 - (max_len % 8));
    if (max_len % 8 == 0) {
        max_len += 8;
    }
    else {
        max_len += 8 - (max_len % 8);
    }

        return max_len;
}

void mx_tab(int len, int maxlen) {
    int count = 0;
    int p = maxlen - len;
    count = (p % 8 != 0) ? ((p / 8) + 1) : (p / 8);

    for (int i = 0; i < count; i++)
        mx_printchar('\t');
}

void mx_print_column(s_list **names, int rows, int num, int maxlen) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; i + j < num; j += rows) {
            mx_printstr(names[i + j]->name);
            if (names[i + j + 1] && (i + j + rows < num))
                mx_tab(mx_strlen(names[i + j]->name), maxlen);
        }
        if (i != rows - 1)
            mx_printchar('\n');
    }
}

void mx_print_names(s_list **names, int maxlen, int w_column) {
    int rows;
    int columns = (w_column / maxlen) != 0 ? w_column / maxlen : 1;
    int num = 0;
    
    while(names[num])
        num++;
    
    if (maxlen * columns > w_column && columns != 1)
        columns--;
    if (num * maxlen > w_column) {
        rows = num / columns;
        if (rows == 0 || num % columns != 0)
            rows += 1;
        mx_print_column(names, rows, num, maxlen);
    } else
        for (int i = 0; names[i]; i++) {
            mx_printstr(names[i]->name);
            if (names[i + 1]) 
                mx_tab(mx_strlen(names[i]->name), maxlen);
        }
        mx_printchar('\n');
}

void mx_out_column(s_list **names) {
    if (!names)
        return;
    struct winsize win;

    int maxlen = mx_len_names(names);
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &win);
    if (isatty(1))
        mx_print_names(names, maxlen, win.ws_col);
    else
        mx_print_names(names, maxlen, 80);
}
