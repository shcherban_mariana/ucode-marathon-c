#include "../inc/uls.h"

// parser for main
void mx_init_flags(s_flags **fl, char flag) {
    if (flag == 'l')
        (*fl)->l = true;
    else if (flag == 'C' || flag == 'x' || flag == 'R' || flag == 'r'
            || flag == 't' || flag == 'u' || flag == 'c' || flag == 'S'
            || flag == 'a' || flag == 'A' || flag == 'm' || flag == 'G'
            || flag == 'T' || flag == 'g' || flag == 'o')
        ;
    else {
        mx_printerr("uls: illegal option -- ");
        write(2, &flag,1);
        mx_printerr("\n");
        mx_printerr("usage: uls [-ACGRSTcfglmortux1] [file ...]\n");
        free(*fl);
        fl = NULL;
        exit(-1);
    }
}

// parser for main
s_flags *mx_check_flags(char *argv[], int *i) {
    s_flags *fl = malloc(sizeof(s_flags));
    while(argv[(*i)]) {
        if (argv[(*i)][0] == '-') {
            if (argv[(*i)][1] == '-') {
                (*i)++;
                break;
            }
            if (argv[(*i)][1] == '\0')
                break;
            for (int j = 1; argv[(*i)][j]; j++)
                mx_init_flags(&fl, argv[(*i)][j]);
        }
        else
            break;

        (*i)++;
    }

    return fl;
}

// for permission 
char mx_acl(s_list *data) {
    acl_t temp;

    if (listxattr(data->path, NULL, 0, XATTR_NOFOLLOW) > 0)
        return ('@');
    if ((temp = acl_get_file(data->path, ACL_TYPE_EXTENDED))) {
        acl_free(temp);
        return ('+');
    }
    return (' ');
}

// for permission 
char mx_get_permission(s_list *data) {
    if (IS_DIR(data->info.st_mode))
        return 'd';
    if (IS_LNK(data->info.st_mode))
        return 'l';
    if (IS_BLK(data->info.st_mode))
        return 'b';
    if (IS_CHR(data->info.st_mode))
        return 'c';
    if (IS_FIFO(data->info.st_mode))
        return 'p';
    if (IS_SOCK(data->info.st_mode))
        return 's';
    if (IS_WHT(data->info.st_mode))
        return 'w';
        
    return '-';
}

char *mx_check_group(s_list *total) {
    struct group *group = getgrgid(total->info.st_gid);
    char *name = NULL;
    
    if (group) {
        name = mx_strdup(group->gr_name);
        return name;
    }
    else {
        name = mx_itoa(total->info.st_gid);
        return name;
    }
}

char *mx_check_pw(s_list *total) {
    struct passwd *pw = getpwuid(total->info.st_uid);
    char *name = NULL;

    if (pw) {
        name = mx_strdup(pw->pw_name);
        return name;
    }
    else {
        name = mx_itoa(total->info.st_uid);
        return name;    
    }
}

void mx_count_size(s_size *size, s_list *total) {
    char *n_group = mx_check_group(total);
    char *n_pw = mx_check_pw(total);

    if (size->link < total->info.st_nlink)
        size->link = total->info.st_nlink;
    if (size->size < total->info.st_size)
        size->size = total->info.st_size;
    if (size->group < mx_strlen(n_group))
        size->group = mx_strlen(n_group);
    if (size->user < mx_strlen(n_pw))
        size->user = mx_strlen(n_pw);

    free(n_group);
    free(n_pw);
}
