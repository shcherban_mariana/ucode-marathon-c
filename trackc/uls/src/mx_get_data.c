#include "../inc/uls.h"

s_list *create_li_node2(char *data) {
    s_list *node = (s_list *)malloc(1 * sizeof(s_list));

    node->name = mx_strdup(data);
    node->path = mx_strdup(data);
    node->error = NULL;
    
    if (lstat(data, &(node->info)) == -1)
        node->error = mx_strdup(strerror(errno));	
    node->open = NULL;

    return node;
}

s_list **mx_create_list(char **name, int count) {
    s_list **new = malloc(count * sizeof(s_list *));
    int i = 0; // obiazatelno

    for (i = 0; name[i]; i++)
        new[i] = create_li_node2(name[i]);
    new[i] = NULL;
    return new;
}

char **names(int argc, char **argv, int i, int *count) {
    int j = i;
    char **names = NULL;

    if (i == argc) {
        *count = 2;
        names = malloc(2 * sizeof(char *));
        names[0] = mx_strdup(".");
        names[1] = NULL;
    }
    else {
        while(argv[j])
            j++;

        names = malloc((j - i + 1) * sizeof(char *));

        for(j = 0; argv[i]; i++, j++)
            names[j] = mx_strdup(argv[i]);

        names[j] = NULL;
        *count = j + 1;
    }
    
    return names;
}

s_list *mx_create_file_node(s_list *arg) {
    s_list *node = (s_list *)malloc(1 * sizeof (s_list));

    node->name = mx_strdup(arg->name);
    node->path = mx_strdup(arg->path);

    if (arg->error)
        node->error = mx_strdup(arg->error);
    else 
        node->error = NULL;

    lstat(node->path, &(node->info));

    if (arg->open != NULL)
        node->open = arg->open;
    else 
        node->open = NULL;
        
    return node;
}

void mx_create_fde(s_list ***files, s_list ***dirs, 
                        s_list ***errors, s_list ***args) {
    int directory = 0;
    int err = 0;
    int j = 0;

    for (int i = 0; (*args)[i] != NULL; i++)
        if ((*args)[i]->error == NULL) {
            if (!IS_DIR((*args)[i]->info.st_mode) ) {
                    j++;
            } else
                directory++;
        } else
            err++;
    if (j > 0)
        *files = malloc((j + 1) * sizeof(s_list *));
    if (directory > 0)
        *dirs = malloc((directory + 1) * sizeof(s_list *));
    if (err > 0)
        *errors = malloc((err + 1) * sizeof(s_list *));
}

void mx_fdir(s_list **args, s_count *num, s_list ***files, s_list ***dirs) {
    if (!IS_DIR((*args)->info.st_mode)) {
        (*files)[num->file++] = mx_create_file_node((*args));
        (*files)[num->file] = NULL;
    }
    else {
        (*dirs)[num->dir++] = mx_create_file_node((*args));
        (*dirs)[num->dir] = NULL;
    }
}

s_list **mx_get_files(s_list ***args, s_flags *fl) {
    s_list **files = NULL;
    s_list **dirs = NULL;
    s_list **errors = NULL;

    s_count *num = malloc(sizeof (s_count));

    num->dir = 0;
    num->err = 0;
    num->file = 0;
    num->i = 0;

    mx_create_fde(&files, &dirs, &errors, args);
    while ((*args)[num->i] != NULL) {
        if ((*args)[num->i]->error == NULL)
            mx_fdir(&(*args)[num->i], num, &files, &dirs);
        else {
            errors[num->err++] = mx_create_file_node((*args)[num->i]);
            errors[num->err] = NULL;
        }

        num->i++;
    }
    if (num->dir > 1)
        fl->files = 1;

    mx_del_listarr(args, dirs);
    mx_out_error(&errors);

    free(num);
    return files;
}

s_list **mx_get_names(int argc, char **argv, int i) {
    int counter = 0;
    char **name = names(argc, argv, i, &counter);
    s_list **args = mx_create_list(name, counter);

    mx_del_strarr(&name);
    return args;
}
