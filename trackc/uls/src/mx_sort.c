#include "../inc/uls.h"

void mx_swap_list(s_list **first, s_list **second) {
    s_list *temp = *first;
    *first = *second;
    *second = temp;
}

void mx_sort(s_list ***data) {
    int size = 0;
	s_list **local_list = *data;
    
    while(local_list[size])
        size++;

	for (int i = 0; i != size; i++) {
		for (int k = i + 1; k != size; k++) {
            if (local_list[i]->error != NULL) {
                if (mx_strcmp(local_list[i]->name, local_list[k]->name) == 1)
                    mx_swap_list(&(local_list[i]), &(local_list[k]));
            }
            else if (mx_strcmp(local_list[i]->name, local_list[k]->name) > 0) {
                mx_swap_list(&(local_list[i]), &(local_list[k]));
		    }
	    }
    }
}
