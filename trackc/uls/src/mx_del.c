#include "../inc/uls.h"

void mx_del_listarr(s_list ***args, s_list **dirs) {
    s_list **array = *args;

    for (int i = 0; array[i]!= NULL; i++) {
        mx_strdel(&array[i]->name);
        mx_strdel(&array[i]->path);

        if (array[i]->error)
            mx_strdel(&array[i]->error);
            
        free(array[i]);
        array[i] = NULL;
    }
    free(*args);
    *args = dirs;
}

void mx_del_arr(s_list ***args) {
    s_list **array = *args;

    for (int i = 0; array[i]!= NULL; i++) {
        mx_strdel(&array[i]->name);
        mx_strdel(&array[i]->path);
        if (array[i]->error)
            mx_strdel(&array[i]->error);
        if (array[i]->open != NULL)
            mx_del_arr(&array[i]->open);
            
        free(array[i]);
        array[i] = NULL;
    }
    
    free(*args);
    *args = NULL;
}
