#ifndef ULS_HEADER_H
#define ULS_HEADER_H

#include "../libmx/inc/libmx.h"
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <sys/acl.h>
#include <sys/ioctl.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <dirent.h>
#include <string.h>

#define IS_BLK(mode) (((mode) & S_IFMT) == S_IFBLK)
#define IS_CHR(mode) (((mode) & S_IFMT) == S_IFCHR)
#define IS_DIR(mode) (((mode) & S_IFMT) == S_IFDIR)
#define IS_LNK(mode) (((mode) & S_IFMT) == S_IFLNK)
#define IS_SOCK(mode) (((mode) & S_IFMT) == S_IFSOCK)
#define IS_FIFO(mode) (((mode) & S_IFMT) == S_IFIFO)
#define IS_WHT(mode) (((mode) & S_IFMT) == S_IFWHT)
#define IS_REG(mode) (((mode) & S_IFMT) == S_IFREG)
#define IS_EXEC(mode) ((mode) & S_IXUSR)


typedef struct flags {
    bool l;
    bool C; // Sorts output vertically in a multicolumn format. This is the default method when output is to a terminal.
    bool x; // Sorts output horizontally in a multi-column format.
    bool R;
    bool r; // Reverses the order of the sort, giving reverse alphabetic or the oldest first, as appropriate.
    bool t; // Sorts by time of last modification (latest first) instead of by name.
    bool u; 
    bool c; 
    bool a;
    bool A;
    bool S;
    bool m; // Uses stream output format (a comma-separated series).
    bool G;
    bool T;
    bool force;
    bool files;
    bool o;
    bool ex;
    bool g; // Displays the same information as the -l flag, except the -g flag suppresses display of the owner and symbolic link information.
}   s_flags;

typedef struct size {
    int link;
    int size;
    int group;
    int user;
    bool isDev;
}   s_size;

typedef struct list {
    char *name;
    char *path;
    char *error;
    struct stat info;
    struct list **open;
}   s_list;

typedef struct count {
    int file;
    int dir;
    int err;
    int i;
}   s_count;


// struct passwd {
// 	char	*pw_name;		/* user name */
// 	char	*pw_passwd;		/* encrypted password */
// 	uid_t	pw_uid;			/* user uid */
// 	gid_t	pw_gid;			/* user gid */
// 	__darwin_time_t pw_change;		/* password change time */
// 	char	*pw_class;		/* user access class */
// 	char	*pw_gecos;		/* Honeywell login info */
// 	char	*pw_dir;		/* home directory */
// 	char	*pw_shell;		/* default shell */
// 	__darwin_time_t pw_expire;		/* account expiration */
// };

// struct group {
// 	char	*gr_name;		/* [XBD] group name */
// 	char	*gr_passwd;		/* [???] group password */
// 	gid_t	gr_gid;			/* [XBD] group id */
// 	char	**gr_mem;		/* [XBD] group members */
// };

// struct winsize {
// 	unsigned short  ws_row;         /* rows, in characters */
// 	unsigned short  ws_col;         /* columns, in characters */
// 	unsigned short  ws_xpixel;      /* horizontal size, pixels */
// 	unsigned short  ws_ypixel;      /* vertical size, pixels */
// };


// typedef struct {
// 	int	__dd_fd;	/* file descriptor associated with directory */
// 	long	__dd_loc;	/* offset in current buffer */
// 	long	__dd_size;	/* amount of data returned */
// 	char	*__dd_buf;	/* data buffer */
// 	int	__dd_len;	/* size of data buffer */
// 	long	__dd_seek;	/* magic cookie returned */
// 	__unused long	__padding; /* (__dd_rewind space left for bincompat) */
// 	int	__dd_flags;	/* flags for readdir */
// 	__darwin_pthread_mutex_t __dd_lock; /* for thread locking */
// 	struct _telldir *__dd_td; /* telldir position recording */
// } DIR;

// mx dir
int mx_count_read(s_list **arg);
s_list *create_li_node1(char *name, char *path);
void mx_open_dir(s_list ***args, s_flags *fl);
void mx_open_dir1(s_list ***args, s_flags *fl);

// mx getfile
s_list *mx_create_file_node(s_list *arg);
void mx_create_fde(s_list ***files, s_list ***dirs, 
                        s_list ***errors, s_list ***args);
void mx_fdir(s_list **args, s_count *num, s_list ***files, s_list ***dirs);
s_list **mx_get_files(s_list ***args, s_flags *fl);

//mx getname
s_list *create_li_node2(char *data);
s_list **mx_create_list(char **name, int count);
char **names(int argc, char **argv, int i, int *count);
s_list **mx_get_names(int argc, char **argv, int i);

//long out(check get)
char mx_acl(s_list *print);
char mx_get_permission(s_list *data);
char *mx_check_group(s_list *total);
char *mx_check_pw(s_list *total);
void mx_count_size(s_size *size, s_list *total);
void mx_del_listarr(s_list ***args, s_list **dirs);

// mx_output
void mx_del_arr(s_list ***args);
void mx_out_menu(s_list ***names, s_flags *fl, int flag);
void mx_output_error(s_list **args, s_flags *fl);
void mx_out_general(s_list ***args, s_flags *fl);
void mx_out_error(s_list ***error);

//mx outputC
int mx_len_names(s_list **names);
void mx_tab(int len, int maxlen);
void mx_print_column(s_list **names, int rows, int num, int maxlen);
void mx_print_names(s_list **names, int maxlen, int w_column);
void mx_out_column(s_list **names);

//mx_parser
void mx_init_flags(s_flags **fl, char flag);
s_flags *mx_check_flags(char *argv[], int *i);
/* main */

//mx_print
void mx_print_permission(s_list *data);
void mx_print_uname(s_list *data, int user);
void mx_print_gname(s_list *data, int group);
void mx_print_size(s_list *data, s_size *size);
void mx_print_time(s_list *data, char *t);
void mx_print_lnk(s_list *data, s_size *size);
void mx_print_subgeneral(s_list *data, s_size *size);
void mx_print_general(s_list **names, int flag);

//mx sort
void mx_swap_list(s_list **bondOne, s_list **bondTwo);
void mx_sort(s_list ***disp);

#endif
