#include "../inc/libmx.h"

static void swap(void **s1, void **s2) {
    void *temp = *s1;
    *s1 = *s2;
    *s2 = temp;
}

s_listst *mx_sors_listst(s_listst *lst, bool (*cmp)(void *, void *)) {
    if (lst && cmp)
        for (s_listst *temp1 = lst; temp1; temp1 = temp1->next)
            for (s_listst *temp2 = lst; temp2->next; temp2 = temp2->next)
                if (cmp(temp2->data, temp2->next->data))
                    swap(&temp2->data, &temp2->next->data);

    return lst;
}
