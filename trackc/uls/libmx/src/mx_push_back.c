#include "../inc/libmx.h"

void mx_push_back(s_listst **list, void *data) {
    if (!*list) {
        *list = mx_create_node(data);
        return;
    }
    s_listst *temp = *list;
    while (temp->next)
        temp = temp->next;
    temp->next = mx_create_node(data);
}
