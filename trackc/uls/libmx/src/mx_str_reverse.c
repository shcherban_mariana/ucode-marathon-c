#include "../inc/libmx.h"

void mx_str_reverse(char *s) {
    int c = mx_strlen(s);
    for (int i = 0; i < c / 2; i++)
        mx_swap_char(&s[i], &s[c - 1 - i]);
}
