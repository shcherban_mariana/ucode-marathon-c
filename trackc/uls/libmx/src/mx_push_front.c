#include "../inc/libmx.h"

void mx_push_front(s_listst **list, void *data) {
    if (!*list) {
        *list = mx_create_node(data);
        return;
    }
    s_listst *temp = mx_create_node(data);
    temp->next = *list;
    *list = temp;
}
