#include "../inc/libmx.h"

int mx_read_line(char **lineptr, size_t buf_size, char delim, const int fd) {
    if (buf_size < 0 || fd < 0)
        return -2;

    (*lineptr) = (char *)mx_realloc(*lineptr, buf_size);
    mx_memset((*lineptr), '\0', malloc_size((*lineptr)));
    size_t bts = 0;
    char buff;

    if (read(fd, &buff, 1)) {
        if (buff == delim)
            return bts;

        (*lineptr) = (char *)mx_realloc(*lineptr, bts + 1);
        (*lineptr)[bts] = buff;
        bts++;
    }
    else
        return -1;

    while (read(fd, &buff, 1)) {
        if (buff == delim)
            break;

        if (bts >= buf_size)
            (*lineptr) = (char *)mx_realloc(*lineptr, bts + 1);

        (*lineptr)[bts] = buff;

        bts++;
    }

    (*lineptr) = (char *)mx_realloc(*lineptr, bts + 1);

    size_t toFreeBts = malloc_size((*lineptr)) - bts;
    mx_memset(&(*lineptr)[bts], '\0', toFreeBts);

    return bts + 1;
}
