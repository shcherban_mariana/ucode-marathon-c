#include "../inc/libmx.h"

void *mx_memchr(const void *s, int c, size_t n) {
    unsigned char *temp = (unsigned char *)s;
    while (n > 0) {
        if (*temp == (unsigned char)c) {
            return temp;
        }
        temp++;
        n--;
    }
    return NULL;
}
