#include "../inc/libmx.h"

void *mx_memmem(const void *big, size_t big_len, const void *little, size_t little_len) {
    unsigned char *buff = NULL;
    unsigned char *obj = NULL;

    if (big_len >= little_len && big_len > 0 && little_len > 0) {
        buff = (unsigned char *)big;
        obj = (unsigned char *)little;
        while (*buff) {
            if (mx_memcmp(buff, obj, little_len - 1) == 0)
                return buff;
            buff++;
        }
    }
    return NULL;
}
