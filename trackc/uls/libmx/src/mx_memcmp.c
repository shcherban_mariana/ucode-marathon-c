#include "../inc/libmx.h"

int mx_memcmp(const void *s1, const void *s2, size_t n) {
    size_t i;
    char *cmps1 = (char *)s1;
    char *cmps2 = (char *)s2;

    for (i = 0; i < n; i++, cmps1++, cmps2++) {
        if (*cmps1 < *cmps2) {
            return *cmps1 - *cmps2;
        }
        else if (*cmps1 > *cmps2) {
            return *cmps1 - *cmps2;
        }
    }
    return 0;
}
