#include "../inc/libmx.h"

void mx_pop_front(s_listst **head) {
    s_listst *temp = NULL;

    if (head == NULL || *head == NULL)
        return;
    if ((*head)->next == NULL) {
        free(*head);
        *head = NULL;
    }
    else {
        temp = (*head)->next;
        free(*head);
        *head = temp;
    }
}
