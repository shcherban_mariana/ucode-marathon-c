#include <unistd.h>

void mx_printchar(char c);

void mx_print_alphabet(void) {
    int j = 0;
    char ch;
    for(int i = 17; i < 43; i++) {
        if(j % 2 == 1) {
            ch = i + 32 + '0';
        }
        else {
            ch = i + '0';
        }
        mx_printchar(ch);
        write(1, "\n", 1);
        j++;
    }
}
