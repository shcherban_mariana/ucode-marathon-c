#include <stdlib.h>

char *mx_strnew(const int size);
char *mx_strcpy(char *dst, const char *src);

char *mx_strdup(const char *s1) {
    int c = 0;
    char i = s1[0];
    while (i != '\0') {
        c++;
        i = s1[c];
    }
    char *duplicate = mx_strnew(c);

    mx_strcpy(duplicate, s1);
    return duplicate;
}
