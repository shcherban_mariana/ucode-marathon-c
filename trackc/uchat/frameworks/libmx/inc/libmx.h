#pragma once

#include <fcntl.h>
#include <locale.h>
#include <stdbool.h>
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <unistd.h>
#include <wchar.h>
#include <libgen.h>

#if __linux__

#include <malloc.h>

#define malloc_size malloc_usable_size
#elif __APPLE__
#include <malloc/malloc.h>
#endif

typedef struct s_list {
    void *data;
    struct s_list *next;
} t_list;

typedef unsigned char uchar_t;

typedef char *string_t;

typedef bool (*cmp_t)(void *, void *);

typedef bool (*is_valid_t)(void *);

// UTILS PACK

int mx_atoi(const char *str);

bool mx_isdigit(char c);

bool mx_isalpha(char c);

bool mx_islower(int c);

bool mx_isupper(int c);

bool mx_isspace(int c);

bool mx_ishex(char c);

char mx_toupper(int c);

char mx_tolower(int c);

void mx_printchar(char c);

void mx_print_unicode(wchar_t c);

void mx_printstr(const char *s);

void mx_printerr(const char *s);

void mx_throwerr(const char *s);

void mx_print_strarr(char **arr, const char *delim);

void mx_printint(int n);

void mx_printul(unsigned long n);

double mx_pow(double n, unsigned int pow);

int mx_sqrt(int x);

char *mx_nbr_to_hex(unsigned long nbr);

unsigned long mx_hex_to_nbr(const char *hex);

char *mx_itoa(int number);

void mx_foreach(int *arr, int size, void (*f)(int));

int mx_binary_search(char **arr, int size, const char *s, int *count);

int mx_bubble_sort(char **arr, int size);

int mx_quicksort(char **arr, int left, int right);

size_t mx_parse_size(const char *size_str, bool *valid);

char *mx_parse_options(int argc, string_t *argv, string_t available_options, void (*error_callback)(char));

void mx_redirect_script_home(char* first_main_arg);

// STRING PACK

int mx_strlen(const char *s);

void mx_swap_char(char *s1, char *s2);

void mx_str_reverse(char *s);

void mx_strdel(char **str);

void mx_del_strarr(char ***arr);

int mx_get_char_index(const char *str, char c);

char *mx_strdup(const char *s1);

char *mx_strndup(const char *s1, size_t n);

char *mx_strcpy(char *dst, const char *src);

bool mx_streq(const char *s1, const char *s2);

bool mx_streqi(const char *s1, const char *s2);

char *mx_strncpy(char *dst, const char *src, int len);

int mx_strcmp(const char *s1, const char *s2);

int mx_strcmpi(const char *s1, const char *s2);

int mx_strncmp(const char *s1, const char *s2, int n);

char *mx_strcat(char *restricts1, const char *restricts2);

char *mx_strchr(const char *s, int c);

char *mx_strstr(const char *haystack, const char *needle);

int mx_get_substr_index(const char *str, const char *sub);

int mx_count_substr(const char *str, const char *sub);

int mx_count_char(const char *str, char c);

int mx_count_words(const char *str, char c);

char *mx_strnew(const int size);

char *mx_strtrim(const char *str);

char *mx_del_extra_spaces(const char *str);

char **mx_strsplit(const char *s, char c);

char **mx_strsplit_extended(const char *s, char c, int splits_count, bool skip_empty);

char **mx_strdivide(const char *s, char c);

char *mx_strjoin(const char *s1, const char *s2);

char *mx_file_to_str(const char *file);

char *mx_replace_substr(const char *str, const char *sub, const char *replace);

int mx_read_line(char **lineptr, size_t buf_size, char delim, const int fd);

char **mx_dup_string_arr(const char **array, const int size);

void mx_del_str_arr(char **array, int size);

// MEMORY PACK

void *mx_memset(void *b, int c, size_t len);

void *mx_memcpy(void *restrictdst, const void *restrictsrc, size_t n);

void *mx_memccpy(void *restrictdst, const void *restrictsrc, int c, size_t n);

int mx_memcmp(const void *s1, const void *s2, size_t n);

void *mx_memchr(const void *s, int c, size_t n);

void *mx_memrchr(const void *s, int c, size_t n);

void *mx_memmem(const void *big, size_t big_len, const void *little,
                size_t little_len);

void *mx_memmove(void *dst, const void *src, size_t len);

void *mx_realloc(void *ptr, size_t size);

void *mx_memjoin_mutation(void **dst, size_t *dst_len, void *src, size_t src_len);

// LIST PACK

t_list *mx_create_node(void *data);

void mx_push_front(t_list **list, void *data);

void mx_push_back(t_list **list, void *data);

void mx_pop_front(t_list **head);

void mx_pop_back(t_list **head);

int mx_list_size(t_list *list);

t_list *mx_sort_list(t_list *lst, cmp_t cmp);

// ARRAYS PACK

bool mx_some(void **arr, size_t len, is_valid_t cmp);

bool mx_every(void **arr, size_t len, is_valid_t cmp);

bool mx_some_is(void **arr, size_t len, cmp_t cmp, void *predicate);

bool mx_every_is(void **arr, size_t len, cmp_t cmp, void *predicate);

void *mx_find(void **arr, size_t len, cmp_t cmp, void *predicate);

size_t mx_find_index(void **arr, size_t len, cmp_t cmp, void *predicate);

void mx_sort(void **arr, size_t len, cmp_t cmp);
