#include "../inc/libmx.h"

void mx_printchar(char c) { write(1, &c, 1); }

void mx_printstr(const char *s) { write(1, s, mx_strlen(s)); }

void mx_printerr(const char *s) { write(STDERR_FILENO, s, mx_strlen(s)); }

void mx_throwerr(const char *s) {
    mx_printerr(s);
    exit(0);
}

bool mx_isdigit(char c) { return c >= '0' && c <= '9'; }

bool mx_isalpha(char c) {
    return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
}

bool mx_islower(int c) { return c >= 'a' && c <= 'z'; }

bool mx_isupper(int c) { return c >= 'A' && c <= 'Z'; }

bool mx_isspace(int c) { return (c >= '\t' && c <= '\r') || c == ' '; }

char mx_tolower(int c) {
    if (mx_isupper(c))
        return c + 32;
    return c;
}

char mx_toupper(int c) {
    if (mx_islower(c))
        return c - 32;
    return c;
}

void mx_printint(int n) {
    if (n == INT_MIN) {
        write(1, "-2147483648", 11);
        return;
    }
    if (n < 0) {
        mx_printchar('-');
        n *= -1;
    }
    if (n < 10) {
        mx_printchar(n + '0');
    } else {
        mx_printint(n / 10);
        mx_printchar(n % 10 + '0');
    }
}

void mx_printul(unsigned long n) {
    if (n < 10) {
        mx_printchar(n + '0');
    } else {
        mx_printint(n / 10);
        mx_printchar(n % 10 + '0');
    }
}

void mx_print_strarr(char **arr, const char *delim) {
    if (!arr || !delim)
        return;
    for (int i = 0; arr[i]; i++) {
        if (i)
            mx_printstr(delim);
        mx_printstr(arr[i]);
    }
    mx_printchar('\n');
}

double mx_pow(double n, unsigned int pow) {
    double res = 1.0;
    for (unsigned int i = 0; i < pow; i++)
        res *= n;
    return res;
}

int mx_sqrt(int x) {
    if (x <= 0)
        return 0;
    float sqrt = x / 2;
    float prev_res = 0;
    while (sqrt != prev_res) {
        prev_res = sqrt;
        sqrt = (x / sqrt + sqrt) / 2;
    }
    return (float)(int)sqrt == sqrt ? (int)sqrt : 0;
}

char *mx_nbr_to_hex(unsigned long nbr) {
    int str_size = 1;
    unsigned long nbr_cpy = nbr;
    while (nbr_cpy >= 16) {
        nbr_cpy /= 16;
        str_size++;
    }
    char *hex = mx_strnew(str_size--);
    while (nbr >= 16) {
        int rest = nbr % 16;
        nbr /= 16;
        hex[str_size--] = rest + (rest < 10 ? '0' : 'W');
    }
    hex[str_size] = nbr + (nbr < 10 ? '0' : 'W');
    return hex;
}

bool mx_ishex(char c) {
    return mx_isdigit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}

unsigned long mx_hex_to_nbr(const char *hex) {
    unsigned long long value = 0;
    unsigned long long prev_value = 0;
    if (hex == NULL) {
        return 0;
    }
    while (mx_ishex(*hex)) {
        prev_value = value;
        value *= 16;
        value += mx_isdigit(*hex)   ? *hex - '0'
                 : mx_islower(*hex) ? *hex - 'a' + 10
                                    : *hex - 'A' + 10;
        if (value <= prev_value)
            return 0;
        ++hex;
    }
    if (*hex != '\0' || value > ULONG_MAX)
        return 0;
    return value;
}

void mx_foreach(int *arr, int size, void (*f)(int)) {
    if (arr == NULL || f == NULL)
        return;
    for (int i = 0; i < size; ++i)
        f(arr[i]);
}

int mx_binary_search(char **arr, int size, const char *s, int *count) {
    (*count) = 0;
    int first = 0;
    int last = size - 1;
    int middle = (first + last) / 2;
    while (first <= last) {
        (*count)++;
        int cmp_res = mx_strcmp(arr[middle], s);
        if (!cmp_res)
            return middle;
        if (cmp_res < 0)
            first = middle + 1;
        else
            last = middle - 1;
        middle = (first + last) / 2;
    }
    if (first > last) {
        *count = 0;
        return -1;
    }
    return -1;
}

int mx_bubble_sort(char **arr, int size) {
    int count = 0;
    for (int i = 0; i < size; i++)
        for (int j = 0; j < size - 1; j++)
            if (mx_strcmp(arr[j], arr[j + 1]) > 0) {
                count++;
                char *temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
    return count;
}

char *mx_itoa(int number) {
    if (number == 0)
        return mx_strdup("0");
    if (number == -2147483648)
        return mx_strdup("-2147483648");
    int length = 0;
    int num_copy = number;
    if (num_copy < 0) {
        length++;
        num_copy *= -1;
    }
    while (num_copy != 0) {
        num_copy /= 10;
        length++;
    }
    char *num_str = mx_strnew(length);
    if (number < 0) {
        num_str[0] = '-';
        number *= -1;
    }
    num_str[length--] = '\0';
    while (number != 0) {
        num_str[length--] = (number % 10) + '0';
        number /= 10;
    }
    return num_str;
}

void mx_print_unicode(wchar_t c) {
    char byte[4];
    if (c < 128) {
        write(1, &c, 1);
    } else if (c < 2048) {
        byte[0] = (c >> 6 & 31) | 192;
        byte[1] = (c >> 0 & 63) | 128;
        write(1, byte, 2);
    } else if (c < 65536) {
        byte[0] = (c >> 12 & 15) | 224;
        byte[1] = (c >> 6 & 63) | 128;
        byte[2] = (c >> 0 & 63) | 128;
        write(1, byte, 3);
    } else {
        byte[0] = (c >> 18 & 7) | 240;
        byte[1] = (c >> 12 & 63) | 128;
        byte[2] = (c >> 6 & 63) | 128;
        byte[3] = (c >> 0 & 63) | 128;
        write(1, byte, 4);
    }
}

int mx_quicksort(char **arr, int left, int right) {
    if (!arr)
        return -1;
    int swaps = 0;
    int middle = mx_strlen(arr[(left + right) >> 1]);
    char *tmp;
    int l = left;
    int r = right;
    while (l <= r) {
        while (mx_strlen(arr[l]) < middle)
            l++;
        while (mx_strlen(arr[r]) > middle)
            r--;
        if (l > r)
            continue;
        if (mx_strlen(arr[l]) != mx_strlen(arr[r])) {
            tmp = arr[l];
            arr[l] = arr[r];
            arr[r] = tmp;
            swaps++;
        }
        l++;
        r--;
    }
    if (r > left)
        swaps += mx_quicksort(arr, left, r);
    if (l < right)
        swaps += mx_quicksort(arr, l, right);
    return swaps;
}

size_t mx_parse_size(const char *size_str, bool *valid) {
    size_t len = mx_strlen(size_str);
    *valid = true;
    for (size_t i = 0; i < len; i++)
        if (!mx_isdigit(size_str[i])) {
            *valid = false;
            return 0;
        }
    while (*size_str == '0') {
        size_str++;
        len--;
    }
    if (!len)
        return 0;
    size_t res = *size_str++ - '0';
    while (*size_str) {
        res *= 10;
        res += *size_str++ - '0';
    }
    return res;
}

char *mx_parse_options(int argc, string_t *argv, string_t available_options, void (*error_callback)(char)) {
    char *opts = (char *)malloc(sizeof(char));
    int size = 1;
    opts[0] = '\0';
    for (int i = 1; i < argc; ++i) {
        if (!argv[i])
            continue;
        string_t opt = argv[i];
        if (*opt != '-')
            continue;
        opt++;
        while (*opt) {
            if (mx_get_char_index(available_options, *opt) == -1) {
                free(opts);
                error_callback(*opt);
                return NULL;
            }
            opts = mx_realloc(opts, size + 1);
            opts[size - 1] = *opt;
            opts[size++] = '\0';
            opt++;
        }
    }
    return opts;
}

int mx_atoi(const char *str) {
    int i = 0;
    int is_minus = 0;
    long long res_res = 0;
    long res = 0;
    while (mx_isspace(str[i]) == 1) {
        i++;
    }
    if (str[i] == '+' || str[i] == '-') {
        if (str[i] == '-')
            is_minus = 1;
        i++;
    }

    while (str[i] == '0')
        i++;

    while (mx_isdigit(str[i]) == 1) {
        res_res = res;
        res = res * 10 + str[i] - '0';
        i++;
        if (res <= res_res) {
            return res * is_minus >= 0 ? 0 : -1;
        }
    }
    if (is_minus == 1) {
        return -res;
    }
    return res;
}

void mx_redirect_script_home(char* first_main_arg) {
    chdir(dirname(first_main_arg));
}
