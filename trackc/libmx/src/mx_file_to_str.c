#include "../inc/libmx.h"

char *mx_file_to_str(const char *filename) {
    if (filename == NULL) {
        return NULL;
    }

    int file = open(filename, O_RDONLY);
    if (file < 0) {
        return NULL;
    }
    char buff;
    int len = 0;

    while (read(file, &buff, 1)) {
        len++;
    }

    if (close(file) < 0) {
        return NULL;
    }

    if (len == 0) {
        return NULL;
    }

    file = open(filename, O_RDONLY);
    if (file < 0) {
        return NULL;
    }
    char *arr = mx_strnew(len);
    read(file, arr, len);
    if (close(file) < 0) {
        return NULL;
    }
    return arr;
}
