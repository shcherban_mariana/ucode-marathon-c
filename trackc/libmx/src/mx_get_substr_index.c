#include "../inc/libmx.h"

int mx_get_substr_index(const char *str, const char *sub) {
    int ind = 0;
    int len = 0;

    if (sub && str) {
        len = mx_strlen(sub);
        while (*str) {
            if (mx_strncmp(str, sub, len) == 0)
                return ind;
            ind++;
            str++;
        }

        return -1;
    }
    return -2;
}
