#include "../inc/libmx.h"

int mx_bubble_sort(char **arr, int size) {
    int temp = 0;
    char *temp_arr;
    for (int i = 0; i < size - 1; i++) {
        for (int j = i + 1; j < size; j++) {
            if (mx_strcmp(arr[i], arr[j]) > 0) {
                temp_arr = arr[i];
                arr[i] = arr[j];
                arr[j] = temp_arr;
                temp++;
            }
        }
    }
    return temp;
}
