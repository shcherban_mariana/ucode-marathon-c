#include "../inc/libmx.h"

int mx_read_line(char **lineptr, size_t buf_size, char delim, const int fd) {
    static char *remainder = NULL;
    char buff[buf_size + 1];
    int toReadBytes = 0;
    int delim_ind = -1;
    char *temp_str = NULL;
    char *temp = NULL;
    if (buf_size == 0)
        return mx_strlen(*lineptr);
    if (remainder)
        temp_str = mx_strdup(remainder);
    while ((toReadBytes = read(fd, buff, buf_size)) >= 0) {
        buff[toReadBytes] = '\0';
        temp = mx_strjoin(temp_str, buff);
        mx_strdel(&temp_str);
        temp_str = mx_strdup(temp);
        mx_strdel(&temp);
        delim_ind = mx_get_char_index(temp_str, delim);
        if (delim_ind != -1) {
            temp_str[delim_ind] = '\0';
            if (remainder) {
                mx_strdel(&remainder);
            }
            remainder = mx_strdup(temp_str + delim_ind + 1);
            break;
        }
        if (toReadBytes == 0) {
            if (remainder)
            {
                mx_strdel(&remainder);
            }
            break;
        }
    }
    if (toReadBytes == -1) {
        return -1;
    }
    *lineptr = mx_strdup(temp_str);
    mx_strdel(&temp_str);
    return mx_strlen(*lineptr);
}
