#include "../inc/libmx.h"

int mx_quicksort(char **arr, int left, int right) {
    int count = 0;
    if (!arr || !*arr)
        return -1;
    if (left < right) {
        int in = left;
        int out = right;

        char *md = arr[in + (out - in) / 2];
        while (in <= out) {
            while (mx_strlen(arr[in]) < mx_strlen(md))
                in++;
            while (mx_strlen(arr[out]) > mx_strlen(md))
                out--;

            if (mx_strlen(arr[in]) != mx_strlen(arr[out])) {
                char *temp = arr[in];
                arr[in] = arr[out];
                arr[out] = temp;
                in++;
                out--;
                count++;
            }
        }
        count += mx_quicksort(arr, left, out);
        count += mx_quicksort(arr, in, right);
    }
    return count;
}
