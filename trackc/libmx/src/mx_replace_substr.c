#include "../inc/libmx.h"

char *mx_replace_substr(const char *str, const char *sub, const char *replace) {
    int lenth;
    char *n;

    if (str && sub && replace && mx_strlen(str) >= mx_strlen(sub)) {
        lenth = mx_strlen(str) + mx_count_substr(str, sub) * (mx_strlen(replace) - mx_strlen(sub));
        n = mx_strnew(lenth);
        for (int i = 0; i < lenth;) {
            if (mx_memcmp(str, sub, mx_strlen(sub)) == 0) {
                str += mx_strlen(sub);
                for (int k = 0; k < mx_strlen(replace); k++)
                    n[i++] = replace[k];
                continue;
            }
            n[i] = *str;
            str++;
            i++;
        }
        return n;
    }
    return NULL;
}
