#include "../inc/ush.h"

static t_env *mx_init_env(void) {
    t_env *env = (t_env *)malloc(sizeof(t_env));
    extern char **environ;
    int size = mx_arr_size(environ) + 1;
    int i = 0;

    env->comm = NULL;
    env->comm_args = NULL;
    env->flag = 0;
    env->env_var = (char **)malloc(size * sizeof(char *));
    for (int j = 0; j < size - 1; j++)
        env->env_var[i++] = mx_strdup(environ[j]);
    env->env_var[i] = NULL;

    return env;
}

static int mx_check_flag1(char **args, int i, t_env *env) {
    if (mx_find_flag("Piu", args[i]) < 3 && env->flag == 0 && args[i + 1] == NULL) {
        fprintf(stderr, "env: option requires an argument -- %c\nusage: env [-i] \
                [-P utilpath] [-u name]\n           \
                [name=value ...] [utility [argument ...]]\n", args[i][1]);
        mx_free_env(env);
        return -1;
    }
    else if (mx_find_flag("Piu", args[i]) == 0) {
        fprintf(stderr, "env: illegal option -- %c\nusage: env [-iv] [-P utilpath] \
                [-u name]\n           [name=value ...] [utility [argument ...]]\n", args[i][1]);
        mx_free_env(env);
        return -1;
    }
    else if (mx_find_flag("Piu", args[i]) > env->flag)
        env->flag = mx_find_flag("Piu", args[i]);

    return 0;
}

static int mx_check_flags(char **args, int i, t_env *env) {//
    if (args[i][0] == '-' && env->flag != 4){
        return mx_check_flag1(args, i, env);
    }
    else if (mx_strcmp(args[i], "--") == 0) {
        env->flag = 4;
        return 0;
    }

    return 1;
}

static void mx_create_comm(t_env *env, char *arg, t_ush *ush) {
    char *temp = NULL;
    int i = 0;

    while (env->env_var[i] != NULL) {
        temp = mx_strdup(env->env_var[i]);
        if (strstr(temp, "PATH=") != NULL) {
            mx_strdel(&temp);
            break;
        }
        mx_strdel(&temp);
        i++;
    }

    if (mx_strcmp(arg, "ush") == 0 || mx_strcmp(arg, "./ush") == 0)
        env->comm = mx_strdup(ush->ush_path);
    if (env->env_var[i] == NULL)
        env->comm = mx_command_in_path(arg, MX_PATH());
    else
        env->comm = mx_command_in_path(arg, env->env_var[i]);
}

t_env *mx_parse_env_args(char **args, t_ush *ush) {
    t_env *env = mx_init_env();
    int len = mx_arr_size(args);
    int env_index = 0;

    for (int i = 1; i < len; i++) {
        if (mx_check_flags(args, i, env) == -1)
            break;
        else if (mx_check_flags(args, i, env) == 0)
            continue;

        mx_create_comm(env, args[i], ush);

        if (mx_execute_env_flags(env, args, i, &env_index) == -1)
            break;
        if (env != NULL)
            mx_strdel(&env->comm);
    }
    
    if (env == NULL)
        return NULL;
    return env;
}
