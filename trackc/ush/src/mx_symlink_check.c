#include "../inc/ush.h"

static void symlink_path(char **arg) {
    char buf[1024];
    ssize_t len = 0;

    if ((len = readlink(*arg, buf, 1024)) == -1)
        perror("readlink");

    buf[len] = '\0';
    mx_strdel(arg);

    if (buf[0] != '.')
        *arg = mx_strjoin("/", buf); 
    else
        *arg = mx_strdup(buf);
}

int mx_symlink_check(char **arg, int flag, int link) {
    struct stat *st = (struct stat *)malloc(sizeof(struct stat));
    int check_link = 0;
    lstat(*arg, st);

    if ((st->st_mode & S_IFMT) == S_IFLNK) {
        check_link = 1;
        if (flag == link)
            symlink_path(arg); 
    }
    
    if (st != NULL) {
        free(st);
        st = NULL;
    }

    // return 1 - is link; 0 - !link
    return check_link;
}
