#include "../inc/ush.h"

// terminal exit processing
static void processing(t_input *input, int *exit_status) {
    // CTRL+Z
    if (input->input_ch == 4) {
        tcsetattr (STDIN_FILENO, TCSANOW, &input->savetty);
        *exit_status = 1;
    }
    // CTRL+C
    else if (input->input_ch == 3)
        input->ctrl_c = 1;
}

// adding an ASCII to a str
char *mx_add_ascii(t_input *input, t_ush *ush) {
    char *str = NULL;

    if (input->input_ch != '\r'  && input->input_ch != '\t' && input->input_ch < 32)
        processing(input, &ush->exit_status);
    else
        str = mx_fill_ch_to_comm(input);

    return str;
}
