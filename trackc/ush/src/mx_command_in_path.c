#include "../inc/ush.h"

// finding the path to the command among the system commands that are in $ PATH
static char *check_in_path(char *command, char **path) {
    char *command_path = NULL;

    // go through directories from the $PATH
    for (int i = 0; i < mx_arr_size(path); i++) {
        command_path = mx_strnew(mx_strlen(command) + mx_strlen(path[i]) + 1);
        mx_strcpy(command_path, path[i]);
        command_path = mx_strcat(command_path, "/");
        command_path = mx_strcat(command_path, command);
        if (mx_file_exist(command_path))
            break;
        mx_strdel(&command_path);
    }

    return command_path;
}

char *mx_command_in_path(char *command, char *str_path) {
    char **path = NULL;
    char *command_path = NULL;
    int paths = 0;
    int count = 0;
    
    if (str_path != NULL) {
        path = mx_strsplit(str_path, ':');
        paths = mx_arr_size(path);
        if (strstr(str_path, "PATH=") != NULL) {
            while(count < mx_strlen(path[0]) - 5) {
                path[0][count] = path[0][count + 5];
                count++;
            }
            path[0][count] = '\0';
        }
        
        if (mx_strcmp(command, "/") != 0 && mx_get_char_index(command, '/') == -1)
            command_path = check_in_path(command, path);

        mx_free_void_arr((void **) path, paths);
    }
    if (command_path == NULL)
        command_path = mx_strdup(command);

    return command_path;
}
