#include "../inc/ush.h"

// from str str remove str str_rm
static char *mx_replace(const char *str, char *str_rm) {
    int len_s = mx_strlen(str);
    int len_strrm = mx_strlen(str_rm);
    int i = 0;
    int j = len_strrm;
    char *name = mx_strnew(len_s - len_strrm);

    while (str[j]) {
        name[i] = str[j];
        i++;
        j++;
    }

    name[i] = '\0';
    return name;
}

// returns 2d array, [0] - str(command), [1] - NULL
static char **case1(char *str_rep) {
    char **result  = (char **)malloc(sizeof(char*) * 2);

    result[0] = mx_strdup(str_rep);
    result[1] = NULL;

    mx_strdel(&str_rep);
    return result;
}

// returns command and smth after it
static char **case2(char *str, char *c) {
    char **result = (char **)malloc(sizeof(char*) * 3);
    int len = mx_get_substr_index(str, c);
    char *temp = mx_strndup(str, len);
    char *str_replace = mx_strndup(str, len + 2);
    char *str_trim = mx_replace(str, str_replace);

    result[0] = temp;
    result[1] = mx_strtrim(str_trim);
    result[2] = NULL;
    
    mx_strdel(&str_replace);
    mx_strdel(&str_trim);
    return result;
}

// split str s | delim c
char **mx_strsplit_custom(const char *s, char *c) {
    char **result = NULL;
    char *str = NULL;
    
    str = mx_to_replace_operator((char *)s);
    
    if (mx_count_queue_operation(str) == 0)
        result = case1(str); // return only command
    else
        result = case2(str, c); // return command and string 

    mx_strdel(&str);
    return result;
}
