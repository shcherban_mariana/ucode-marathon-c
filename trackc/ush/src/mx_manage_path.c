#include "../inc/ush.h"

static int mx_cd_prev_err(char *path, t_ush *ush, char *val) {
    mx_ush_setenv(ush->pwd, ush);

    if (mx_file_exist(path) == 0)
        fprintf(stderr, "cd: no such file or directory: %s\n", path);
    else
        fprintf(stderr, "cd: not a directory: %s\n", path);

    mx_strdel(&val);
    return 1;
}

static char *mx_prev_dir(t_ush *ush, int flag) {
    char *prev_dir = NULL;

    if (flag != 1)
        prev_dir = mx_strdup(ush->pwd_l);
    else
        prev_dir = MX_PWD();

    for (int i = mx_strlen(prev_dir) - 1; i >= 0; i--) {
        if (prev_dir[i] == '/') {
            prev_dir[i] = '\0';
            break;
        }
        prev_dir[i] = '\0';
    }

    if (mx_strlen(prev_dir) == 0) {
        mx_strdel(&prev_dir);
        prev_dir = mx_strdup("/");
    }

    return prev_dir;
}

static char *mx_abs_path(char *path, t_ush *ush) {
    char *temp = NULL;
    char *val = NULL;

    if (mx_strcmp(ush->pwd_l, "/") != 0) { 
        temp = mx_strjoin(ush->pwd_l, "/");
        val = mx_strjoin(temp, path);
        mx_strdel(&temp);
    }
    else
        val = mx_strjoin("/", path);

    return val;
}

static int mx_create_new_path(char **signs, char *path, t_ush *ush, int flag) {
    char *val = NULL;

    for (int i = 0; signs[i] != NULL; i++) {
        if (mx_strcmp(signs[i], "..") == 0)
            val = mx_prev_dir(ush, flag);
        else if (mx_strcmp(signs[i], ".") == 0) {
            if (flag != 1)
                val = mx_strdup(ush->pwd_l);
            else
                val = MX_PWD();
        }
        else
            val = mx_abs_path(signs[i], ush);

        if (chdir(val) != -1)
            mx_ush_setenv(val, ush);
        else
            return mx_cd_prev_err(path, ush, val);

        mx_strdel(&val);
    }
    return 0;
}

int mx_create_path(char *path, t_ush *ush, int flag) {
    char **signs = mx_strsplit(path, '/');
    int res = 0;
    ush->pwd = mx_strdup(ush->pwd_l);

    if (path[0] == '/') {
        chdir("/"); //root
        mx_ush_setenv("/", ush); 
    }
    if (signs != NULL)
        res = mx_create_new_path(signs, path, ush, flag);

    setenv("OLDPWD", ush->pwd, 1);
    mx_del_strarr(&signs);
    mx_strdel(&ush->pwd);
    return res;
}
