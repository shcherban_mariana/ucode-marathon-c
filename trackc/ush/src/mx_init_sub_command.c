#include "../inc/ush.h"

t_com_sub* mx_init_sub_command() {
    t_com_sub *sub_comm = (t_com_sub *) malloc(sizeof (t_com_sub));

    sub_comm->back_first = 0;
    sub_comm->back_first_index = 0;
    sub_comm->back_end = 0;
    sub_comm->back_end_index = 0;
    sub_comm->space = 0;
    sub_comm->space_first_index = 0;
    sub_comm->space_end_index = 0;
    sub_comm->temp_str = NULL;
    sub_comm->temp_data = NULL;
    sub_comm->cout_execute = NULL;
    sub_comm->temp_join = NULL;
    sub_comm->status = 0;
    return sub_comm;
}
