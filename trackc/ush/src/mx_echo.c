#include "../inc/ush.h"

void mx_skip_chars(int i, char *str, char *parsed_str, int index) {
    if (str[i] == 'a')
        parsed_str[index] = '\a';
    else if (str[i] == 'n')
        parsed_str[index] = '\n';
    else if (str[i] == 'b')
        parsed_str[index] = '\b';
    else if (str[i] == 't')
        parsed_str[index] = '\t';
    else if (str[i] == 'r')
        parsed_str[index] = '\r';
    else if (str[i] == 'v')
        parsed_str[index] = '\v';
    else if (str[i] == 'f')
        parsed_str[index] = '\f';
    else
        parsed_str[index] = str[i];
}

int mx_quote_err(char **parsed_str, int *flag_n, int fg_quote) {
    if (fg_quote == -1) {
        *flag_n = -1;
        mx_strdel(parsed_str);
        fprintf(stderr, "Odd number of quotes.\n");
        return -1;
    }
    return 0;
}

char *mx_fill_parsed_str(char *str, int *flag_n, int flag) {
    char *parsed_str = mx_strnew(1000);
    int len = mx_strlen(str);
    int fg_quote = 1;
    int index = 0;
    bool one_flag = 0;
    bool double_quote =  0;
    
    fflush(stdout);
    for (int i = 0; str[i] != '\0' && i < len; i++) {
        if (((str[i] == '\'') || (str[i]== '\\'&& str[i + 1] == '\'' )) && one_flag == 1){
            fg_quote *= -1;
            one_flag  = !one_flag;
        }
        else if(str[i] == '\'' && str[i-1] != '\\' && one_flag == 0 && !double_quote){
            fg_quote *= -1;
            one_flag = !one_flag;
        }
        else if( ((str[i] == '\"' && str[i-1] != '\\') || (str[i] == '\"' 
        && str[i - 1] == '\\' &&  str[i - 2] == '\\')) && one_flag == 0) {
            fg_quote *= -1;
            double_quote = !double_quote;
        }
        else if (flag == 1 && str[i] == '\\' && fg_quote == -1)
            mx_skip_chars(++i, str, parsed_str, index++);
        else if (str[i] == '\\' && fg_quote == 1)
            parsed_str[index++] = str[++i];
        else
            parsed_str[index++] = str[i];
    }

    parsed_str[index] = '\0';
    if (mx_quote_err(&parsed_str, flag_n, fg_quote) == -1)
        *flag_n = -1;

    mx_strdel(&str);
    return parsed_str;
}

static int mx_flags_echo(char *data, int *flag_n) {
    int flag = 1;

    for (int i = 1; i < mx_strlen(data); i++) {
        if (data[i] == 'e' && flag != 2)
            flag = 1;
        else if (data[i] == 'E' || data[i] == 'e')
            flag = 2;
        else if (data[i] == 'n')
            *flag_n = 1;
        else {
            flag = -1;
            break;
        }
    }
    return flag;
}

char *mx_parse_echo(char **args, int *flag_n, char* temp) {
    char *str = NULL;
    char *temp_str = mx_strdup(temp);
    char *temp_str2 = temp_str;
    bool checked_space = false;
    int flag = 1;

    for (int i = 1; i < mx_arr_size(args); i++) {
        if (args[i][0] == '-' && str == NULL && flag != -1) {
            flag = mx_flags_echo(args[i], flag_n);
            if (flag >= 0)
                continue;
        }
        temp_str = &temp_str[mx_get_substr_index(temp_str, args[i]) + 1];
        
        if(temp_str[mx_strlen(args[i]) - 1] == ' ')
            checked_space = true;

        if (str == NULL && mx_strcmp(args[i], "") != 0){
            str = strdup(args[i]);
            if(checked_space)
                strcat(str, " ");
        }
        else if (mx_strcmp(args[i], "") != 0) {
            str = realloc(str,strlen(args[i]) + strlen(str) + 2);
            
            strcat(str, args[i]);
            if(checked_space)
                strcat(str, " ");
        }
        temp_str = &temp_str[mx_strlen(args[i]) - 1];
        
        checked_space = false;
    }
    free(temp_str2);
    temp_str2 = NULL;
    
    if (str != NULL)
        str = mx_fill_parsed_str(str, flag_n, flag);

    return str;
}

int mx_echo(char **args, char *temp) {
    int flag = 0;
    
    char *str = mx_parse_echo(args, &flag, temp);
    
    if (str != NULL)
        mx_printstr(str);
    if (flag == 0)
        mx_printchar('\n');
    else if (flag == -1)
        return 1;
    
    mx_strdel(&str);
    return 0;
}
