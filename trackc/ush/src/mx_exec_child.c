#include "../inc/ush.h"

static void mx_child_non_builtin(int *res_val, char **input, char *command_p) {
    extern char **environ;
    DIR *dp = NULL;
    
    if ((dp = opendir(command_p)) != NULL) {
        fprintf(stderr, "ush: %s: is a directory\n", input[0]);
        *res_val = 1;
        closedir(dp);
    }
    else if (mx_file_exist(command_p)) {
        for (int i = 0; i < mx_arr_size(input); i++)
            input[i] = mx_fill_parsed_str(input[i], 0, 0);

        int exec = execve(command_p, input, environ);
        if (exec == -1 && errno == EACCES) {
            fprintf(stderr, "ush: Permission denied:%s\n", input[0]);
            *res_val = 1;
        }
    }
    else {
        fprintf(stderr, "ush: %s: command not found\n", input[0]);
        *res_val = 1;
    }
}

static void mx_child_free(char *command_p, int *fd, int res_val) {
    char *res = NULL;
    res = mx_itoa(res_val);
    mx_write_to_pipe(res, fd);

    mx_strdel(&res);
    mx_strdel(&command_p);
}

void mx_exec_child(int *res, char **input, t_redirect *red, t_ush *ush) {
    char *command_p = mx_command_in_path(input[0], getenv("PATH"));
    int command = mx_check_builtin_command(command_p);
    mx_child_ancestor(red);

    if (command == 2)
        *res = mx_pwd(input, ush);
    else if (command == 3)
        *res = mx_env(input, ush);
    else if (command == 8)
        *res = mx_which(input);
    else if (command == 9)
        *res = mx_echo(input, ush->str_input);
    else if (command == 0 || command == 4) {
        if (mx_strcmp(command_p, "ush") == 0 || mx_strcmp(command_p, "./ush") == 0) {
            mx_strdel(&input[0]);
            input[0] = mx_strdup(ush->ush_path);
        }
        mx_child_non_builtin(res, input, command_p);
    }
    mx_child_free(command_p, red->fd_return, *res);

    tcsetpgrp(1,ush->parent_pid);
    exit(0);
}
