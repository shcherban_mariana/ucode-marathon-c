#include "../inc/ush.h"

void mx_insert_char(t_input *input, char sym, int index) {
    char temp;
    int len = mx_strlen(input->command);
    
    input->command = realloc(input->command, len + 2);
    while (index < len + 1){
        temp = input->command[index];
        input->command[index] = sym;
        sym = temp;
        index++;
    }
    input->command[index] = '\0';
}


void mx_delete_char(t_input *input, int index) {
    int len =  mx_strlen(input->command);

    input->command = realloc(input->command, len);
    while (index < len - 1) {
        input->command[index] = input->command[index + 1];
        index++;
    }
    input->command[index] = '\0';

}
