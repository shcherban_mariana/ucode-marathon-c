#include "../inc/ush.h"

int mx_file_exist(char *path) {
    struct stat *s = (struct stat*)malloc(sizeof (struct stat));
    int flag = 0;

    if ((lstat(path, s) != -1))
        flag = 1;
        
    if (s != NULL) {
        free(s);
        s = NULL;
    }

    return flag;
}
