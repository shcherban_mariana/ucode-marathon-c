#include "../inc/ush.h"

char *mx_strcopyn(char *dst, const char *src, int start, int finish) {
    int i = start;
    int j = 0;

    while (src[i] && i != finish) {
        dst[j] = src[i];
        i++;
        j++;
    }
    
    return dst;
}
