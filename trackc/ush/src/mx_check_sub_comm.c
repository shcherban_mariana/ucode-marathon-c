#include "../inc/ush.h"

void mx_check_sub_comm(char **data, t_ush *ush) {
    t_com_sub *sub_comm = mx_init_sub_command();
    int exit = 0;
    bool check_grave = false;
    bool check_dollar = false;
    bool qoute = false;
    int *qoute_arr = NULL;
    int qoute_num = 0;
    int sub_dollar = 0;

    //loop for processing a command char-by-char (search for nested commands)
    for (int i = 0; (*data)[i] != '\0'; i++) {
        if (!check_dollar && mx_check_grave_sub_comm(data, sub_comm, ush, i, &check_grave, &sub_dollar, &qoute, &qoute_num, &qoute_arr) == 1) {
            exit = 1;
            break;
        }
        if (!check_grave && mx_check_sub_dollar_comm(data, sub_comm, ush, i, &check_dollar, &sub_dollar, &qoute, &qoute_num, &qoute_arr) == 1) {
            exit = 1;
            break;
        }
    }

    if (sub_comm != NULL)
        free(sub_comm);
    if (exit == 1)
        mx_check_sub_comm(data, ush);
}
