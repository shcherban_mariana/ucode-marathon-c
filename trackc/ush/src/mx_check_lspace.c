#include "../inc/ush.h"

void mx_check_lspace(char **str) {
    bool endmost = false;
    char *ptr = "\0";

    if(mx_get_substr_index(*str, "\\") != -1){
            ptr = &((*str)[mx_get_substr_index(*str, "\\")]);
            endmost = true;
    }
    while(*ptr != '\0' && mx_get_substr_index(*str, "\\") != -1){
        if(*ptr == ' ' && endmost)
            endmost = true;
        else if(*ptr == '\\' && *(ptr+1) == ' '){
            endmost = true;
            ptr++;
        }
        else
            endmost = false;
        ptr++;
    }

    if((*str)[mx_get_substr_index(*str, "\\")+1] == ' ' && endmost){
        *str = mx_strtrim(*str);
        *str = mx_strjoin(*str, " ");
    }
    else
        *str = mx_strtrim(*str);
}
