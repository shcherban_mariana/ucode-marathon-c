#include "../inc/ush.h"

static void sigint(){
    mx_printstr("\n");
}

int main(int argc, char **argv) {
    if (argc > 1) {
        fprintf(stderr, "ush: can't open input file: %s\n", argv[1]);
        exit(127);
    }

    t_ush *ush = mx_init_ush(argv);
    mx_init_shell_env();

    while (true) {
        signal(SIGINT, sigint);
        signal(SIGTSTP, SIG_IGN);

        //reading the current line
        ush->command = mx_read_str(ush);

        //recording a command from a line in the order of execution
        t_queue **queue = mx_parse_str(ush);
        
        if(queue != NULL){
            // execute commands
            ush->return_value = mx_exec_str(queue, ush);
            free(queue);
        }

        mx_strdel(&ush->command);
        if (ush->exit_status != -1 || ush->exit_non_term == 1)
            break;
    }
 
    mx_free_ush(ush);
    if (ush->exit_status != -1)
        exit(ush->exit_status);

    return ush->return_value;
}
