#include "../inc/ush.h"

void mx_insert_sub_command(t_com_sub *c, char **data, t_ush *ush, char *symbol) {
    if ((*data)[c->back_first_index] == '$' && (*data)[c->back_first_index+1] == '(')
        c->temp_str = mx_strdupn(*data,c->back_first_index + 2, c->back_end_index);
    else if ((*data)[c->space_first_index] == '`')
        c->temp_str = mx_strdupn(*data, c->space_first_index + 1, c->space_end_index);
    else
        c->temp_str = mx_strdup(*data);

    c->status = mx_exec(ush, c->temp_str, 1, &c->cout_execute);

    if (mx_strcmp(symbol, "$") == 0) { 
        c->temp_join = mx_strjoin("$(", c->temp_str);
        mx_strdel(&c->temp_str);
        c->temp_str = mx_strjoin(c->temp_join, ")");
    }
    else if (mx_strcmp(symbol, "`") == 0) {
        c->temp_join = mx_strjoin("`", c->temp_str);
        mx_strdel(&c->temp_str);
        c->temp_str = mx_strjoin(c->temp_join, "`");
    }
}
