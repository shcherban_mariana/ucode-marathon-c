#include "../inc/ush.h"

static int mx_par_exec_comm(char **input, t_ush *ush, pid_t pid) {
    int res_val = 0;
    char *command_p = mx_command_in_path(input[0], MX_PATH());
    int command = mx_check_builtin_command(command_p);
    
    ush->curr_pid = pid;
    if (command == 1)
        res_val = mx_cd(input, ush);
    else if (command == 5)
        res_val = mx_export(input);
    else if (command == 6)
        res_val = mx_unset(input);
    else if (command == 7)
        res_val = mx_exit(input, &ush->exit_status);
    mx_strdel(&command_p);

    return res_val;
}

static void mx_check_redirect(int *res, int *fd, t_redirect *red, t_ush *ush, pid_t pid) {
    char *ret_str = mx_strnew(1);
    int status = 0;
    mx_swap_pids(ush);
    waitpid(ush->curr_pid, &status, WUNTRACED);

    if(WIFEXITED(status))
        *res = WEXITSTATUS(status);
    else
        if(WIFSIGNALED(status))
            *res = WTERMSIG(status);

    if(!WIFSTOPPED(status)) {
            mx_read_from_pipe(ret_str, 1, fd);
            mx_parent_ancestor(red, res);
    }
    if(WIFSTOPPED(status)) {
        tcsetpgrp(1,ush->parent_pid);
        mx_push_pid(pid, &ush);
        signal(SIGTTIN, SIG_DFL);
        signal(SIGTTOU, SIG_DFL);
        *res = 146;
        printf("\nush: suspended  %s\n", ush->str_input);
    }
    else if (*res == 1 || mx_atoi(ret_str) == 1)
        *res = 1;
    else
        *res = 0;

    mx_strdel(&ret_str);
}

int mx_exec(t_ush *ush, char *str_input, int flag_redir, char **str_red) {
    mx_check_lspace(&str_input);

    char **input = mx_check_extension(&str_input, ush->return_value);
    pid_t pid;
    int return1 = 0;
    t_redirect *redirect = mx_init_ancestor(flag_redir);
    ush->str_input = str_input;
    signal(SIGINT, SIG_DFL);
    signal(SIGTSTP, SIG_DFL);
    signal(SIGTTIN, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
    
    pid = fork();
    if (pid != 0) {
        return1 = mx_par_exec_comm(input, ush, pid);
        mx_check_redirect(&return1, redirect->fd_return, redirect, ush, pid);
        tcsetpgrp(1, ush->parent_pid);
    }
    else {
        setpgid(getpid(), getpid());
        tcsetpgrp(1, getpid());
        mx_exec_child(&return1, input, redirect, ush);
    }
    
    if (redirect->_stdout != NULL && flag_redir == 1) {
        if (redirect->_stdout[mx_strlen(redirect->_stdout) - 1] == '\n')
            redirect->_stdout[mx_strlen(redirect->_stdout) - 1] = '\0';

        *str_red = mx_strdup(redirect->_stdout);
    }
    
    mx_free_execute(redirect, input);
    return return1;
}
