#include "../inc/ush.h"

char **mx_to_split_by_space(char *s, char delim) {
    int count = 1; 
    int len = mx_strlen(s);

    for (int i = 0; i != len; i++)
        if (s[i] == delim && s[i - 1] != '\\' && s[i + 1] != delim && s[i + 1] != '\0')
            count++;

    char **result = (char **)malloc(count * sizeof(char *) + 1);
    int k = 0;
    int start = -1;

    for(int i = 0; i < len && k < count; i++){
        for(int i = 0; i < len; i++){
            if(s[i] == delim && s[i-1] != '\\' && start != -2){
                result[k] = mx_strndup(&(s[start+1]), i - start-1);
                k++;
                if(s[i+1] != delim && s[i+1] != '\0'){
                    start = i;
                }
                else
                    start = -2;
                
            }
            if(start == -2 && s[i+1] != delim && s[i+1] != '\0'){
                start = i;
            }
            if(start != i && start != -2 && s[i+1] == '\0'){
                if(i - start > 1)
                    result[k] = mx_strndup(&(s[start+1]), i - start);
                else
                    result[k] = mx_strndup(&(s[start+1]), 1);
                k++;
                start = -1;
            }
        } 
    }

    result[count] = NULL;
    return result;
}
