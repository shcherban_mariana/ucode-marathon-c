#include "../inc/ush.h"

char **mx_echo_split(char *src, char delim) {
    char *src_ptr = src;
    // ident single quote
    // ident double quote
    // ident non quotes string
    bool start_f = false;
    bool start_s = false;
    int start1 = 0;
    int start2 = 0;
    int start3 = -1; 
    int finish_f = 0;
    int finish_s = 0;
    int finish3 = 0;
    // word counter
    int counter = 0;

    for(int i = 0; i < mx_strlen(src); i++){
        // check on single quotes
        if (((src_ptr[i] == '\'' && src_ptr[i - 1] != '\\') || (src_ptr[i] == '\'' && src_ptr[i - 1] == '\\' &&  src_ptr[i - 2] == '\\')) && start_f == false && start_s != true)
            start_f = true;
        else if (src_ptr[i] == '\'' && start_f == true && start_s != true){
            counter++;
            start_f = false;
        }

        // check on double quotes
        if (((src_ptr[i] == '\"' && src_ptr[i - 1] != '\\') || (src_ptr[i] == '\"' && src_ptr[i - 1] == '\\' &&  src_ptr[i - 2] == '\\')) && start_s == false && start_f != true)
            start_s = true;
        else if(((src_ptr[i] == '\"' && src_ptr[i - 1] != '\\') || (src_ptr[i] == '\"' && src_ptr[i - 1] == '\\' &&  src_ptr[i - 2] == '\\')) && start_s == true && start_f != true){
            counter++;
            start_s = false;
        }

        // check on nonquotes
        if (start_f == false && start_s == false && start3 == -1 && ((src_ptr[i] != delim && src_ptr[i] != '\'' && src_ptr[i] != '\"') 
        || (src_ptr[i] == delim && src_ptr[i] == '\'' && src_ptr[i] == '\"' && src_ptr[i-1] == '\\')))
            start3 = i;

        if (start_f == false && start_s == false && start3 != -1 && (((src_ptr[i + 1] == '\'' || src_ptr[i + 1] == '\"') && src_ptr[i] != '\\') || src_ptr[i + 1] == '\0')){
            finish3 = i;
            if (finish3 != start3) {
                char *temp = mx_strndup(&(src[start3]), finish3 - start3 + 1);
                counter += mx_arr_size(mx_to_split_by_space(temp, delim));
                free(temp);
                temp = NULL;
            }
            else
                counter++;
            start3 = -1;
        }

        if ((start_f || start_s) && src_ptr[i+1] == '\0')
            counter++;
    }

    start_f = false;
    start_s = false;
    char **res = (char **)malloc((counter+1) * sizeof(char *));

    for (int i = 0, j = 0; i < mx_strlen(src) && j < counter; i++){
        // search and insert single quote
        if (((src_ptr[i] == '\'' && src_ptr[i - 1] != '\\') || (src_ptr[i] == '\'' && src_ptr[i - 1] == '\\' &&  src_ptr[i - 2] == '\\')) && start_f == false && start_s != true) {
            start_f = true;
            start1 = i;
        }
        else if (src_ptr[i] == '\'' && start_f == true && start_s != true) {
            start_f = false;
            finish_f = i;
            res[j] = strndup(&(src_ptr[start1]), finish_f - start1 + 1);
            j++;
        }

        // search and insert double quote
        if (((src_ptr[i] == '\"' && src_ptr[i-1] != '\\') || (src_ptr[i] == '\"' && src_ptr[i - 1] == '\\' &&  src_ptr[i - 2] == '\\')) && start_s == false && start_f != true) {
            start_s = true;
            start2 = i;
        }
        else if( ((src_ptr[i] == '\"' && src_ptr[i - 1] != '\\') || (src_ptr[i] == '\"' && src_ptr[i - 1] == '\\' &&  src_ptr[i - 2] == '\\')) && start_s == true && start_f != true) {
            start_s = false;
            finish_s = i;
            res[j] = strndup(&(src_ptr[start2]), finish_s - start2 + 1);
            j++;
        }

        // search and insert simple string
        if (start_f == false && start_s == false && start3 == -1 && ((src_ptr[i] != delim && src_ptr[i] != '\'' && src_ptr[i] != '\"') 
        || (src_ptr[i] == delim && src_ptr[i] == '\'' && src_ptr[i] == '\"' && src_ptr[i - 1] == '\\')))
            start3 = i;

        if (start_f == false && start_s == false && start3 != -1 && (((src_ptr[i + 1] == '\'' || src_ptr[i + 1] == '\"') && src_ptr[i] != '\\') || src_ptr[i + 1] == '\0')) {
            finish3 = i;
            if (finish3 != start3) {
                char **temp = mx_to_split_by_space(mx_strndup(&(src[start3]), finish3 - start3 + 1), delim);
                for (int k = 0; k < mx_arr_size(temp); k++, j++)
                    res[j] = mx_strndup(temp[k], mx_strlen(temp[k]));

                mx_free_void_arr((void **)temp, mx_arr_size(temp));
            }
            else {
                res[j] = mx_strndup(&(src[i]),1);
                j++;
            }
            start3 = -1;
        }

        if (start_f && src[i + 1] == '\0'){
            if (start1 != i)
                res[j] = mx_strndup(&(src[start1]), i - start1 + 1);
            else
                res[j] = mx_strndup(&(src[i]),1);
            j++;
        }

        if (start_s && src[i + 1] == '\0'){
            if (start2 != i)
                res[j] = mx_strndup(&(src[start2]), i - start2 + 1);
            else
                res[j] = mx_strndup(&(src[i]),1);
            j++;
        }
    }

    res[counter] = NULL;
    return res;
}

