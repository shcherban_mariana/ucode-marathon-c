#include "../inc/ush.h"

static char *mx_cur_path_ush(char **commands){
    char *pwd = MX_PWD();
    char *ush_path = NULL;

    if (mx_strstr(commands[0], "./"))
        ush_path = mx_replace_substr(commands[0], ".", pwd);
    else
        ush_path = mx_strdup(commands[0]);

    mx_strdel(&pwd);
    return ush_path;
}

t_ush* mx_init_ush(char **argv) {
    t_ush *ush = (t_ush *)malloc(sizeof(t_ush));

    ush->parent_pid = getpid();
    ush->command = NULL;
    ush->exit_status = -1;
    ush->return_value = 0;
    ush->exit_non_term = 0;
    ush->pids = NULL;
    ush->str_input = NULL;
    ush->ush_path = mx_cur_path_ush(argv);
    ush->pwd_l = MX_PWD();

    return ush;
}

void mx_init_shell_env(void) {
    char *shlv = MX_SHLVL();
    char *shlvl = mx_itoa(mx_atoi(shlv) + 1);
    extern char **environ; //necessarily
    char cwd[PATH_MAX];

    if (getenv("HOME") == NULL)
        setenv("HOME", MX_HOME(), 1);
    if (getcwd(cwd, sizeof(cwd)) != NULL)
        setenv("PWD", cwd, 1);
    if (getenv("OLDPWD") == NULL)
        setenv("OLDPWD", MX_PWD(), 1);
    if (getenv("PATH") == NULL)
        setenv("PATH", MX_PATH(), 1);
    if (getenv("SHLVL") == NULL)
        setenv("SHLVL", "1", 1);
    else
        setenv("SHLVL", shlvl, 1);

    setenv("_", "/usr/bin/env", 1);
    mx_strdel(&shlvl);
    mx_strdel(&shlv);
}

void mx_free_ush(t_ush *ush) {
    if (ush != NULL) {
        mx_strdel(&ush->ush_path);
        mx_strdel(&ush->pwd_l);
        free_pids(ush->pids);
        free(ush);
    }
}
