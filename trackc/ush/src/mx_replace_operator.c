#include "../inc/ush.h"

char *mx_to_replace_operator(char *s) {
    char *temp1 = mx_strdup(s);
    char *temp2 = NULL;
    
    if (mx_get_substr_index(temp1," && ")  >= 0)
        temp2 = mx_replace_substr(temp1, " && ", "&&");
    else if (mx_get_substr_index(temp1," || ")  >= 0)
        temp2 = mx_replace_substr(temp1, " || ", "||");
    else
        temp2 = mx_strdup(temp1);

    mx_strdel(&temp1);
    return temp2;
}
