#include "../inc/ush.h"

int mx_exit(char **input, int *exit_status) {
    int result = 0;

    if (mx_arr_size(input) == 1)
        *exit_status = 0;
    else if (mx_atoi(input[1]) == -2147483648 && mx_arr_size(input) == 2) {
        fprintf(stderr, "ush: exit: %s: numeric argument required\n", input[1]);
        result = 1;
    }
    else if (mx_arr_size(input) == 2)
        *exit_status = mx_atoi(input[1]);
    else {
        fprintf(stderr, "ush: exit: too many arguments\n");
        result = 1;
    }

    return result; 
}
