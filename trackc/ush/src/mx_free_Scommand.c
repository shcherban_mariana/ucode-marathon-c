#include "../inc/ush.h"

void mx_free_Scommand(t_com_sub *sub_comm) {
    if (sub_comm != NULL) {
        mx_strdel(&sub_comm->temp_join);
        mx_strdel(&sub_comm->temp_str);
        mx_strdel(&sub_comm->cout_execute);
    }
}
