#include "../inc/ush.h"

void mx_push_pid(pid_t pid, t_ush **ush) {
    if ((*ush)->pids != NULL) {
        t_pid *temp = (t_pid *)malloc(sizeof(t_pid));
        temp->index = (*ush)->pids->index + 1;
        temp->num = pid;
        temp->prev = NULL;
        temp->str = mx_strdup((*ush)->str_input);
        temp->next = (*ush)->pids;
        (*ush)->pids = temp;
    }
    else
    {
        (*ush)->pids = (t_pid *) malloc(sizeof (t_pid));
        (*ush)->pids->num = pid;
        (*ush)->pids->index = 1;
        (*ush)->pids->str = mx_strdup((*ush)->str_input);
        (*ush)->pids->prev = NULL;
        (*ush)->pids->next = NULL;
    }
}

void mx_swap_pids(t_ush *ush) {
    t_pid *temp = NULL;
    char **input = mx_strsplit(ush->str_input, ' ');
    

    if (mx_strcmp(input[0], "fg") == 0 && ush->pids != NULL) {
        mx_strdel(&ush->str_input);
        ush->curr_pid = ush->pids->num;
        ush->str_input = mx_strdup(ush->pids->str);
        temp = ush->pids;
        ush->pids = ush->pids->prev;
        mx_strdel(&temp->str);
        free(temp);
    }

    mx_free_void_arr((void **) input, mx_arr_size(input));
}

void free_pids(t_pid *pids) {
    t_pid *temp = NULL;

    if (pids != NULL) {
        while (pids->prev != NULL) {
            temp = pids;
            pids = pids->prev;
            mx_strdel(&temp->str);
            free(temp);
        }
        mx_strdel(&pids->str);
        free(pids);
    }
}
