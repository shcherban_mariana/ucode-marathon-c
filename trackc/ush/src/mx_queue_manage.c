#include "../inc/ush.h"

static int mx_get_fg_ind(const char *str, const char *sub) {
    if (!str || !sub)
        return -1;

    for (int i = 0; i <= mx_strlen(str) - mx_strlen(sub); i++)
        if (!mx_strncmp(&str[i], sub, mx_strlen(sub)))
            return i;

    return INT_MAX;
}

static char *push_back_queue(char *operation, char *temp, t_queue **arr_queue) {
    char **str_arr = mx_strsplit_custom(temp, operation);
    mx_queue_push_back(arr_queue, str_arr[0], operation[0]);
    mx_strdel(&temp);
    temp = mx_strdup(str_arr[1]);
    mx_free_void_arr((void **)str_arr, mx_arr_size(str_arr));

    return temp;
}

void mx_queue_insert(char *array, t_queue **arr_queue) {
    int or = 0;
    int and = 0;
    char *temp = mx_strdup(array);
    int count = mx_count_queue_operation(array);
    
    for (int i = 0; i <= count; i++) {
        and = mx_get_fg_ind(temp, "&&");
        or = mx_get_fg_ind(temp, "||");
        if ((and >= 0) && (and < or))
            temp = push_back_queue("&&", temp, arr_queue);
        else if ((or >= 0) && (or < and ))
            temp = push_back_queue("||", temp, arr_queue);
        else if (and == INT_MAX && or == INT_MAX) {
            if (temp == NULL)
                temp = mx_strdup(array);
            mx_queue_push_back(arr_queue, temp, '0'); //  
        }
    }
    
    mx_strdel(&temp);
}

int mx_count_queue_operation(const char *array) {
    int count = 0;
    if (array)
        for (int i = 0; array[i + 1] != '\0'; i++)
            if ((array[i] == '&' && array[i + 1] == '&') || (array[i] == '|' && array[i + 1] == '|'))
                count++;

    return count;
}

t_queue *mx_create_queue(char *data, char operation) {
    if (!data)
        return NULL;
    t_queue *new = (t_queue *)malloc(sizeof(t_queue));
    if (!new)
        return NULL;
    new->data = mx_strdup(data);
    new->operator = operation;
    new->next = NULL;

    return new;
}

void mx_queue_pop_front(t_queue **queue) {
    t_queue *temp = NULL;

    if (queue == NULL || *queue == NULL)
        return;
    if ((*queue)->next == NULL) {
        free((*queue)->data);
        free(*queue);
        *queue = NULL;
    }
    else {
        temp = (*queue)->next;
        free((*queue)->data);
        free(*queue);
        *queue = temp;
    }
}

void mx_queue_push_back(t_queue **queue, char *data, char operation) {
    t_queue *temp = NULL;
    t_queue *last_list = mx_create_queue(data, operation);

    if (*queue == NULL)
        *queue = last_list;
    else {
        temp = *queue;
        while (temp->next != NULL)
            temp = temp->next;
        temp->next = last_list;
    }
}
