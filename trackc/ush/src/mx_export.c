#include "../inc/ush.h"
 
int mx_export(char **args) {
    char *val = NULL;
    char *name = NULL;
    int l_name = 0;
    int l_val = 0;

    for (int i = 1; i < mx_arr_size(args); i++) {
        args[i] = mx_fill_parsed_str(args[i], 0, 1);
        l_name = mx_get_char_index(args[i], '=');
        if (l_name != -1) {
            l_val = mx_strlen(args[i]) - l_name - 1;
            name = mx_strnew(l_name);
            val = mx_strnew(l_val);

            for (int i = 0; *(args[i]) != '='; i++)
                name[i] = *(args[i]);
            for (int i = 0; args[l_name + i + 1] != (void * )0; i++)
                val[i] = *(args[l_name + i + 1]);

            setenv(name, val, 1);
            mx_strdel(&name);
            mx_strdel(&val);
        }
    }
    return 0;
}

int mx_unset(char **args) { 
    for (int y = 1; y < mx_arr_size(args); y++)
        unsetenv(args[y]);
    return 0;
}
