#include "../inc/ush.h"

static char *change_dollar(int len, char **input) {
    char *var = NULL;

    if ((*input)[1] == '{' && (*input)[len - 1] == '}') {
        var = mx_strnew(len - 1);
        for (int i = 2, j = 0; i < len - 1; i++, j++)
            var[j] = (*input)[i];
        var[len - 3] = '=';
        var[len - 2] = '\0';
    }
    else {
        var = mx_strnew(len + 1);
        for (int i = 1, j = 0; i < len; i++, j++)
            var[j] = (*input)[i];
        var[len - 1] = '=';
        var[len] = '\0';
    }
    return var;
}

static void insert_str(char **input, int j, char *var, char *replace) {
    extern char **environ;
    char *val = mx_strndup(&(environ[j][mx_strlen(var)]), mx_strlen(environ[j]) - mx_strlen(var) + 1);
    *input = mx_replace_substr(*input, replace, val);
}

static void checked_dollar(char **input, bool edge_element) {
    extern char **environ;
    char *ptr_input = *input;
    int flag = 0;
    int start_finish = 0;
    int index = 0;
    
    while(*ptr_input != '$'){
        index++;
        ptr_input++;
    }
    
    int i = index;
    while (i < mx_strlen(*input) && (*input)[i] != ' ' && start_finish != 2) {
        if((*input)[i] == '{' || (*input)[i] == '}')
            start_finish++;
        i++;
    }

    int len = i - index;
    char * temp = mx_strndup(ptr_input, len);
    char *var = change_dollar(i - index, &temp);
    
    for (int j = 0; environ[j] != NULL; j++) {
        if (strstr(mx_str_tolower(environ[j]), mx_str_tolower(var)) != NULL && mx_tolower(environ[j][0]) == mx_tolower(var[0])) {
            flag = 1;
            insert_str(input, j, var, temp);
            break;
        }
    }
    
    if(!edge_element){
        if (flag == 0) {
            mx_strdel(input);
            (*input) = mx_strdup("");
        }
    }
    else
        *input = mx_replace_substr(*input, temp, "");
    
    mx_strdel(&var);
    mx_strdel(&temp);
}

// echo $SHLVL $_ $USER $SHELL $HOME
char **mx_check_extension(char **str, int res_val) {
    int len = 0;
    char **input = mx_echo_split(*str, ' ');
    
    for (int i = 0; i <= mx_arr_size(input); i++) {
        char **temp = NULL;
        bool checked = true;
        bool tilde = false;
        bool edge_element = false;

        if (i != mx_arr_size(input))
            temp = &(input[i]);
        else {
            temp = str;
            edge_element = true;
        }
        
        while(checked){
            checked = false;
            if (mx_get_substr_index(*temp, "$?") >= 0) {
                mx_strdel(&(*temp));
                *temp = mx_itoa(res_val);
                checked = true;
            }
            
            if (!tilde && mx_get_substr_index(*temp, "~") >= 0){
                mx_check_tilde(temp);
                tilde = true;
                checked = true;
            }
            len = mx_strlen(*temp);
            int index_dollar = mx_get_substr_index(*temp, "$");
            
            if (index_dollar >= 0 && (*temp)[index_dollar - 1] != '\\'){
                checked_dollar(&(*temp), edge_element);
                checked = true;
            }
        }
    }
    return input;
}
