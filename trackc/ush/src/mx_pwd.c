#include "../inc/ush.h"

static void mx_cd_error(char c, int *flag) {
    if (c != '\0') 
        fprintf(stderr, "ush: pwd: -%c: invalid option\npwd: usage: pwd [-LP]\n", c);
    else
        fprintf(stderr, "pwd: too many arguments\n");
    *flag = 3;
}

static void mx_parse_pwd(char **args, int *flag) {
    int len = mx_arr_size(args); 
    int stop = 0;

    for (int i = 1; i < len; i++) {
        if (mx_strcmp(args[i], "--") == 0)
            stop = 3;
        if (stop == 0 && args[i][0] == '-') {
            for (int j = 1; j < mx_strlen(args[i]); j++) {
                if (args[i][j] != 'L' && args[i][j] != 'P') {
                    mx_cd_error(args[i][j], flag);
                    break;
                }
            }
            if (*flag == 3)
                break;
            if((*flag = mx_find_flag("LP", args[i])) > 0)
                continue;
        }
        else
            mx_cd_error('\0', flag);
    }
}

int mx_pwd(char **args, t_ush *ush) {
    int flag = 0;
    int checked_link = 0;
    char *pos = NULL;

    mx_parse_pwd(args, &flag);

    if (flag != 2)
        pos = mx_strdup(ush->pwd_l);
    else {
        pos = MX_PWD();
        checked_link = mx_symlink_check(&pos, flag, 2);
    }

    if (flag != 3) {
        mx_check_lspace(&pos);
        mx_printstr(pos);
        mx_printchar('\n');
    }
    else {
        mx_strdel(&pos);
        return 1;
    }

    mx_strdel(&pos);
    return 0;
}
