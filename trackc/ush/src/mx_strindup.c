#include "../inc/ush.h"

char *mx_strdupn(const char *data, int start, int finish) {
    char *new = NULL;

    if (!data)
        return NULL;
        
    new = mx_strnew(finish - start);
    return mx_strcopyn(new, data, start, finish);
}
