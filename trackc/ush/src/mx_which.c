#include "../inc/ush.h"

static int checked_comm_resword(char *str) {
    char reserved[][7] = {"!", "{", "}", "time", "export"};

    for (int i = 0; i < 5; i++)
        if (strcmp(str, reserved[i]) == 0)
            return 1;

    return -1;
}

static void mx_builtin_check(char *input, int *res_flag, int flag) {
    char *temp = mx_command_in_path(input, MX_PATH());
    int builtin_command = mx_check_builtin_command(temp);

    if (builtin_command != 0) {
        printf("%s: shell built-in command\n", input);
        if (flag == 2 && strcmp(temp, input) != 0 && flag != 1)
            printf("%s\n", temp);
    }
    else if (strcmp(temp, input) != 0) { 
        if (flag != 1)
            printf("%s\n", temp);
    }
    else{
        fprintf(stderr, "%s not found\n", input);
        *res_flag = 1;
    }
    mx_strdel(&temp);
}

static void mx_check_comm(char *input, int *res_flag, int flag) {
    char **path = NULL;
    
    // reserved word/char check | 1 - true -1 false
    if (checked_comm_resword(input) == 1)
        printf("%s: shell reserved word\n", input);
    else if (mx_get_char_index(input, '/') != -1) { 
        path = mx_strsplit(MX_PATH(), ':');
        for (int i = 0; i < mx_arr_size(path); i++) {
            if (mx_file_exist(input) && mx_get_substr_index(input, path[i]) != -1) {
                if (flag != 1)
                    printf("%s\n", input); 
                *res_flag = 0;
                break;
            }
            *res_flag = 1;
        }
        mx_free_void_arr((void**)path, mx_arr_size(path));
    }
    else
        mx_builtin_check(input, res_flag, flag);
}

static int mx_check_flags(char *input, int *flag) {
    for (int i = 1; i < mx_strlen(input); i++) {
        if (input[i] == 's' && *flag != 2)
            *flag = 1;
        else if (input[i] == 'a')
            *flag = 2;
        else {
            fprintf(stderr, "which: illegal option -- \
                    %c\nusage: which [-as] program ...\n", input[i]);
            return 1;
        }
    }

    return 0;
}

int mx_which(char **input) {
    int res_flag = 0;
    int flag = 0;

    if (mx_arr_size(input) == 1) {
        fprintf(stderr, "usage: which [-as] program ...\n");
        return 1;
    }
    for (int i = 1; i < mx_arr_size(input); i++) {
        if (strcmp(input[i], "--") == 0)
            flag = 3;
        else if (input[i][0] == '-' && flag < 3)
            if ((res_flag = mx_check_flags(input[i], &flag)) == 0) 
                continue;
        mx_check_comm(input[i], &res_flag, flag); 
    }

    return res_flag;
}
