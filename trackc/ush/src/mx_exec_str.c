#include "../inc/ush.h"

static void case_or(t_queue **queue, int i) {
    mx_queue_pop_front(&queue[i]);

    while (queue[i] != NULL && queue[i]->operator == '|')
        mx_queue_pop_front(&queue[i]);

    if (queue[i] != NULL)
        mx_queue_pop_front(&queue[i]);
}

static void case_and(t_queue **queue, int i) {
    mx_queue_pop_front(&queue[i]);
    
    while (queue[i] != NULL && (queue[i]->operator == '&' || queue[i]->operator == '0'))
        mx_queue_pop_front(&queue[i]);
}

static bool str_is_empty(char *data) {
    bool status = false;
    char *temp = mx_strdup(data);
    
    if (mx_strlen(temp) != 0)
        status = true;
    mx_strdel(&temp);

    return status;
}

int mx_exec_str(t_queue **queue, t_ush *ush) {
    int status = 0;
    for (int i = 0; queue[i] != NULL; i++) {
        while (queue[i] != NULL) {
            mx_check_sub_comm(&queue[i]->data, ush);

            if (str_is_empty(queue[i]->data) == true)
                status = mx_exec(ush, queue[i]->data, 0, NULL);
            if (ush->exit_status != -1) {
                mx_queue_pop_front(&queue[i]);
                return status;
            }
            
            if (status == 0 && queue[i]->operator == '&') {
                mx_queue_pop_front(&queue[i]);
                continue;
            }
            else if (status == 0 && queue[i]->operator == '|') {
                case_or(queue, i);
                continue;
            }
            else if (status == 1 && queue[i]->operator == '&') {
                case_and(queue, i);
                continue;
            }
            else
                mx_queue_pop_front(&queue[i]);
        }
    }

    return status;
}
