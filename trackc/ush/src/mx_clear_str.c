#include "../inc/ush.h"

void mx_clear_str() {
    write(STDOUT_FILENO, "\033[2K", 4);
    //return the cursor back
    mx_printstr("\033[G");
}
