#include "../inc/ush.h"

char *mx_fill_ch_to_comm(t_input *input) {
    char *str = NULL;
    
    if(input->input_ch == MX_BACKSPACE) {
        if (input->coursor_position > 0) {
            input->coursor_position--;
            input->len--;
            mx_clear_str();
            if (input->term_width > input->len + 8)
                mx_printstr("u$h> ");
            mx_delete_char(input, input->coursor_position);
            if (input->term_width > input->len + 8)
                mx_printstr(input->command);
        }
    }
    else if(input->input_ch == MX_ENTER) {
        str = strdup(input->command);
        mx_check_lspace(&str);
    }
    else {
        if (input->len + 8 > input->term_width) {
            mx_printerr("\nush: input string is too long");
            input->term_width = 0;
        }
        else {
            if (input->coursor_position != input->len)
                mx_insert_char(input, input->input_ch, input->coursor_position);
            else if (input->command != NULL) {
                input->command = realloc(input->command, input->len + 2);
                input->command[input->len] = (char) input->input_ch;
                input->command[input->len + 1] = '\0';
            }
            else {
                input->command = mx_strnew(1000); 
                input->command[input->len] = (char) input->input_ch;
            }

            input->len++;
            input->coursor_position++;

            if (input->coursor_position != input->len) {
                mx_clear_str();
                mx_printstr("u$h> ");
                mx_printstr(input->command);
            }
            else
                mx_printchar((char) input->input_ch);
        }
    }
    return str;
}
