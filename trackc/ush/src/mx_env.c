#include "../inc/ush.h"

static int running_command(t_env *env, char **args, int i) {
    int len = 0;

    env->comm_args = mx_strdup(env->comm);
    for (int j = i + 1; args[j] != NULL; j++) {
        len =  mx_strlen(args[j]) + mx_strlen(env->comm_args) + 2;
        env->comm_args = realloc(env->comm_args, len);
        mx_strcat(env->comm_args, " ");
        mx_strcat(env->comm_args, args[j]);
    }

    return -1;
}

static void P_flag(t_env *env, char *arg) { 
    for (int i = 0; env->env_var[i]!= NULL; i++) {
        if (strstr(env->env_var[i], "PATH=") != NULL) {
            mx_strdel(&env->env_var[i]);
            env->env_var[i] = mx_strjoin("PATH=", arg);
            break;
        }
    }
}

static int i_flag(t_env *env, char **args, int i, int *env_in) { 
    if (mx_get_char_index(args[i], '=') == 0) {
        fprintf(stderr, "env: setenv %s: Invalid argument\n", args[i]);
        mx_free_env(env);
        return -1;
    }

    if (args[i - 1][0] == '-') {
        env->env_var = (char **)malloc(sizeof(char *) * 2);
        env->env_var[(*env_in)++] = mx_strdup(args[i]);
        env->env_var[*env_in] = NULL;
    }
    else {
        env->env_var = realloc(env->env_var, (*env_in + 2) * sizeof(char *));
        env->env_var[(*env_in)++] = mx_strdup(args[i]);
        env->env_var[*env_in] = NULL;
    }

    return 0;
}

static int u_flag(t_env *env, char *arg, char **environ) {
    int i = 0;

    if (mx_get_char_index(arg, '=') >= 0) {
        fprintf(stderr, "env: unsetenv %s: Invalid argument\n", arg);;
        mx_free_env(env);
        return -1;
    }
    env->env_var = malloc(1 * sizeof(char *));
    char *temp = mx_strjoin(arg, "=");

    for (int j = 0; environ[j]!= NULL; j++) {
        if (strstr(environ[j], temp) == NULL) {
            env->env_var = realloc(env->env_var, (i + 2) * sizeof(char*));
            env->env_var[i++] = mx_strdup(environ[j]);
        }
    }
    env->env_var[i] = NULL;
    mx_strdel(&temp);
    return 0;
}

int mx_execute_env_flags(t_env *env, char **args, int i, int *env_index) {
    extern char **environ;

    if (env->flag == 2 && args[i - 1][0] == '-')
        mx_del_strarr(&env->env_var);

    if (env->comm != NULL && mx_strcmp(args[i],env->comm) != 0)
        return running_command(env, args, i);
    else if (env->flag == 1 && args[i - 1][0] == '-' && mx_file_exist(args[i]) == 1)
        P_flag(env, args[i]);
    else if (env->flag == 2 && mx_get_char_index(args[i], '=') >= 0)
        return i_flag(env, args, i, env_index);
    else if (env->flag == 3 && args[i - 1][0] == '-')
        return u_flag(env, args[i], environ);
    else if (mx_strcmp(args[i], "./ush") != 0){
        mx_env_err(env, args, i);
        return -1;
    }

    return 0;
}

void mx_free_env(t_env *env) {
    if (env != NULL) {
        mx_del_strarr(&env->env_var);
        mx_strdel(&env->comm_args);
        mx_strdel(&env->comm);
        free(env);
    }
}

void mx_env_err(t_env *env, char **args, int i) {
    if (mx_file_exist(args[i]) != 1)
        fprintf(stderr, "env: %s: No such file or directory\n", args[i]);
    else if (mx_file_exist(args[i]) == 1)
        fprintf(stderr, "env: %s: Permission denied\n", args[i]);

    mx_free_env(env);
}

int mx_env(char **args, t_ush *ush) {
    char **env_args = NULL;
    t_env *env = mx_parse_env_args(args, ush);
    
    if (env != NULL) {
        if (env->comm == NULL || mx_strcmp(env->comm, "env") == 0)
            mx_print_strarr(env->env_var, "\n");
        else if (env->comm != NULL && ush != NULL) {
            env_args = mx_strsplit(env->comm_args, ' ');
            if (fork())
                wait(NULL);
            else {
                execve(env_args[0], env_args, env->env_var);
                exit(1);
            }
            mx_free_void_arr((void **)env_args, mx_arr_size(env_args));
        }

        mx_free_env(env);
        return 0;
    }

    return 1;
}
