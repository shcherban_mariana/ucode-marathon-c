#include "../inc/ush.h"

t_queue **mx_parse_command(char *command) {
    char **arr = mx_strsplit(command, ';');
    int size = mx_arr_size(arr);
    t_queue **array_queue = (t_queue **)malloc(sizeof(t_queue *) * (size + 1));

    for (int i = 0; i < size; i++) {
        array_queue[i] = NULL;
        //mx_check_lspace(&arr[i]);
        mx_queue_insert(arr[i], &array_queue[i]);
    }
    mx_free_void_arr((void **)arr, size);
   
    array_queue[size] = NULL;
    return array_queue;
}

t_queue** mx_parse_str(t_ush *ush) {
    t_queue **queue = NULL;
    if (ush->command != NULL && strlen(ush->command) > 0)
        queue = mx_parse_command(ush->command);
    
    return queue;
}
