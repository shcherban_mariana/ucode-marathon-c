#include "../inc/ush.h"

void static mx_find_sub_comm(t_com_sub *sub_comm, char **data, int i,  bool *check_grave, int *sub_dollar, bool *qoute, int *qoute_num, int **quote_array) {
    if ((*data)[i] == '$' && (*data)[i + 1] == '(' && (*data)[i-1] != '\\' && *check_grave) {
        (*sub_dollar)++;
        *qoute = false;
    }

    if ((*data)[i] == ')' && (*data)[i-1] != '\\' && *check_grave && !(*qoute)) {
        (*sub_dollar)--;
        if((*quote_array) != NULL && (*quote_array)[(*qoute_num) - 1] == (*sub_dollar))
            *qoute = true;
    }
    
    if ((*data)[i] == '`' && (*data)[i - 1] != '\\' && sub_comm->space == 0 && *sub_dollar == 0) {
        *check_grave = true;
        *qoute = false;
        sub_comm->space++;
        sub_comm->space_first_index = i;
    }
    else if ((*data)[i] == '`' && (*data)[i - 1] != '\\' && sub_comm->space == 1 && *sub_dollar == 0) {
        *check_grave = false;
        sub_comm->space++;
        sub_comm->space_end_index = i;

        if ((*quote_array)!=NULL && (*quote_array)[(*qoute_num) - 1] == (*sub_dollar))
            *qoute = true;
    }

    if( (*data)[i] == '"' && (*data)[i - 1] != '\\'){
        *qoute = !(*qoute);
        if (!(*qoute)) {
            (*qoute_num)--;

            if ((*qoute_num) == 0) {
                 free(*quote_array);
                 quote_array = NULL;
            }
            else
                *quote_array = (int *)realloc(*quote_array, sizeof(int) * (*qoute_num));
        }
        else {
            (*qoute_num)++;

            if ((*qoute_num) == 1)
                *quote_array = (int *)malloc(sizeof(int) * (*qoute_num));
            else
                *quote_array = (int *)realloc(*quote_array, sizeof(int) * (*qoute_num));

            if ((*check_grave) == true)
                (*quote_array)[(*qoute_num) - 1] = (*sub_dollar);
            else
                (*quote_array)[(*qoute_num) - 1] = 0;
        }
    }

    if ((*data)[i] == '\'') {
        if (*qoute)
            *qoute = !(*qoute);
        else if ((*data)[i - 1] != '\\')
            *qoute = !(*qoute);
        
        if (!(*qoute)) {
            (*qoute_num)--;

            if ((*qoute_num) == 0) {
                 free(*quote_array);
                 quote_array = NULL;
            }
            else
                *quote_array = (int *)realloc(*quote_array, sizeof(int) * (*qoute_num));
        }
        else {
            (*qoute_num)++;

            if ((*qoute_num) == 1)
                *quote_array = (int *)malloc(sizeof(int) * (*qoute_num));
            else
                *quote_array = (int *)realloc(*quote_array, sizeof(int) * (*qoute_num));

            if ((*check_grave) == true)
                (*quote_array)[(*qoute_num) - 1] = (*sub_dollar);
            else
                (*quote_array)[(*qoute_num) - 1] = 0;
        }
    }
}

static bool check_space_fail(t_com_sub *c, int i)  {
    if (c->space_first_index == i - 1 && c->space_end_index == i) {
        c->space_first_index = 0;
        c->space_end_index = 0;
        return true;
    }

    return false;
}

// searchin nested command, which with `
int mx_check_grave_sub_comm(char **data, t_com_sub *c, t_ush *ush, int i, bool *check_grave, int *sub_dollar, bool *qoute, int *qoute_num, int **quote_array) {
    mx_find_sub_comm(c, data, i, check_grave, sub_dollar, qoute, qoute_num, quote_array);
    
    if (c->space_first_index < c->space_end_index && !(*check_grave)) {
        if (check_space_fail(c, i) == true)
            return 0;

        bool check_extra_sub_comm = false;
        // checking on nested command
        for (int i = c->space_first_index + 1; i < c->space_end_index; i++){
            if ((*data)[i] == '$' && (*data)[i - 1] != '\\' && (*data)[i + 1] == '(')
                check_extra_sub_comm = true;
            if ((*data)[i] == '`' && (*data)[i - 1] != '\\')
                check_extra_sub_comm = true;
            if (check_extra_sub_comm)
                break;
        }

        char *inside_comm = strndup(&(*data)[c->space_first_index + 1],  c->space_end_index - c->space_first_index - 1);
                
        if (check_extra_sub_comm){
            char *inside_sub_comm = strndup(&((*data)[c->space_first_index + 1] ), c->space_end_index - c->space_first_index - 1);
                    
            mx_check_sub_comm(&inside_comm, ush);
                    
            *data = mx_replace_substr(*data, inside_sub_comm, inside_comm);
                    
            char *parsed_inside_comm = mx_strjoin(mx_strjoin("`", inside_comm), "`");
            c->space_first_index = 0;
            c->space_end_index = mx_strlen(parsed_inside_comm) - 1;

            mx_insert_sub_command(c, &parsed_inside_comm, ush, "`");
        }
        else
            mx_insert_sub_command(c, data, ush, "`");
    
        c->cout_execute = mx_replace_substr(c->cout_execute, "\n", " ");   
        mx_check_lspace(&c->cout_execute);

        if (c->cout_execute == NULL)
            *data = mx_replace_substr(*data, c->temp_str, "");
        else
             *data = mx_replace_substr(*data, c->temp_str, c->cout_execute);

        if (c->temp_data != NULL) {
            mx_strdel(data);
            *data = c->temp_data;
            return 1;
        }
        if(*data != NULL)
            return 1;
    }

    return 0;
}
