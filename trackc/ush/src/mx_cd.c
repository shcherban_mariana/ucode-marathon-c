#include "../inc/ush.h"

void mx_ush_setenv(char *data, t_ush *ush) {
    if (mx_strlen(data) != 1 && data[mx_strlen(data) - 1] == '/')
        data[mx_strlen(data) - 1] = '\0';
    if (mx_strcmp(data, ush->pwd_l) != 0) {
        mx_strdel(&ush->pwd_l);
        ush->pwd_l = mx_strdup(data);
    }
    setenv("PWD", data, 1);
}

static void set_oldpwd(t_ush *ush) {
    mx_ush_setenv(MX_OLDPWD(), ush);
    setenv("OLDPWD", ush->pwd_l, 1);
}

static int handle_path(char *path, int flag, t_ush *ush) {
    int res = 0;
    char *temp = mx_strdup(path);
    int link = mx_symlink_check(&temp, flag, 1);

    if (flag == 2 && link == 1) {
        fprintf(stderr, "cd: not a directory: %s\n", path);
        res = 1;
    }
    else if (mx_strcmp(temp, "-") == 0) {
        if (MX_OLDPWD() != NULL)
            set_oldpwd(ush);
        else {
            fprintf(stderr, "ush: cd: OLDPWD not set\n");
            res = 1;
        }
    }
    else
        res = mx_create_path(temp, ush, flag);
    
    mx_strdel(&temp);
    return res;
}

// go to dir and keep the previous dir
static void current_pos(t_ush *ush) {
    char *data = MX_HOME();
    setenv("OLDPWD", ush->pwd_l, 1);
    mx_ush_setenv(data, ush);
    mx_strdel(&data);
}

int mx_cd(char **args, t_ush *ush) {
    int terminate = 0;
    int flag = 0;
    int len = mx_arr_size(args);

    for (int i = 0; i < mx_arr_size(args); i++)
        args[i] = mx_fill_parsed_str(args[i], 0, 0);

    if (len == 1 || (mx_strcmp(args[1], "--") == 0 && len == 2))
        current_pos(ush);
    else {
        for (int i = 1; i < len; i++) {
            if (mx_strcmp(args[i], "--") == 0) {
                terminate = 3;
                continue;
            }
            if (terminate == 0 && args[i][0] == '-')
                if ((flag = mx_find_flag("Ps", args[i])) > 0)
                    continue;

            return handle_path(args[i], flag, ush);
        }
    }

    return 0;
}
