#include "../inc/ush.h"

static t_input *init_input() {
    t_input *input = (t_input *) malloc(sizeof (t_input));

    input->len = 0;
    input->command = mx_strnew(1);
    input->ctrl_c = 0;
    input->coursor_position = 0;
    input->input_ch = '\0';
    input->input_ch_arr = (char *)&input->input_ch;

    return input;
}

static char *mx_parse_ch(t_input *input, t_ush *ush, char *str) {
    int j = 0;
    int i = read(0, &input->input_ch, 4);
    
    while (j < i) {
        input->input_ch = input->input_ch_arr[j];
        if (input->input_ch <= 127 && input->input_ch != 27) {
            str = mx_add_ascii(input, ush);
            if (ush->exit_status != -1)
                break;
        }
        if (input->input_ch < 32)
            break;
        j++;
    }

    return str;
}

static char *mx_parse_termline(struct termios savetty, t_ush *ush) {
    char *str = NULL;
    char *temp = NULL;
    t_input *input = init_input();
    input->savetty = savetty;

    if (tgetent(NULL, "xterm-256color") < 0) {
        fprintf(stderr, "ush: Could not access the termcap data base.\n");
        exit(1);
    }
    input->term_width = tgetnum("co");
    
    while (input->input_ch != '\r' && input->ctrl_c != 1 && input->term_width != 0) {
        str = mx_parse_ch(input, ush, str);
        if (ush->exit_status != -1)
            break;
    }
    
    mx_free_step(input, temp);
    mx_printstr("\n");
    return str;
}

// reading from terminal to string
char *mx_read_str(t_ush *ush) {
    char *str = NULL;
    struct termios savetty;
    size_t buf_size = 32;
    char *buffer = NULL;

    if (!isatty(0)) {
        getline(&buffer, &buf_size, stdin);
        str = mx_strndup(buffer, mx_strlen(buffer) - 1);
        ush->exit_non_term = 1;
        mx_strdel(&buffer);
    }
    else {
        mx_printstr("u$h> ");
        mx_set_non_basic(&savetty);
        str = mx_parse_termline(savetty, ush);
        
        tcsetattr (STDIN_FILENO, TCSANOW, &savetty);
    }
    
    return str;
}
