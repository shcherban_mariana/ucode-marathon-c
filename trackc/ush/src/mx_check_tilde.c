#include "../inc/ush.h"

static void mx_replace_tilde(char **input, char *sub, char *rep) {
    char *temp = mx_strdup(*input);
    *input = mx_replace_substr(temp, sub, rep);
    mx_strdel(&temp);
}

static void mx_checked_tilde(char **input, char *home, char *pwd) {
    if (mx_isalpha((*input)[1]))
        mx_replace_tilde(input, "~", "/Users/");
    else if ((*input)[1] == '/' || (*input)[1] == '\0')
        mx_replace_tilde(input, "~", home);
    else if ((*input)[1] == '+' && ((*input)[2] == '/' || (*input)[2] == '\0'))
        mx_replace_tilde(input, "~+", pwd);
    else if ((*input)[1] == '-' && ((*input)[2] == '/' || (*input)[2] == '\0'))
        if (MX_OLDPWD() != NULL)
            mx_replace_tilde(input, "~-", MX_OLDPWD());
}

void mx_check_tilde(char **input) {
    char *pwd = MX_PWD();
    char *home = mx_strdup("");
    
    if (getenv("HOME") != NULL) {
        mx_strdel(&home);
        home = MX_HOME();
    }

    mx_checked_tilde(input, home, pwd);
    mx_strdel(&home);
    mx_strdel(&pwd);
}
