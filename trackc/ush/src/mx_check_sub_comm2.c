#include "../inc/ush.h"

// search for the beginning and end of var $ (...)
void static mx_find_sub_comm(t_com_sub *sub_comm, char **data, int i, bool *check_dollar, int *sub_dollar, bool *qoute, int *qoute_num, int **quote_array) {
    if ((*data)[i] == '$' && (*data)[i + 1] == '(' && (*data)[i-1] != '\\') {
        *qoute = false;
        if (!(*check_dollar)) {
            sub_comm->back_first++;
            sub_comm->back_first_index = i;
            *check_dollar = true;
        }
        else
            (*sub_dollar)++;
    }

    if (*check_dollar && (*data)[i] == ')' && (*data)[i-1] != '\\' && !(*qoute)) {
        if (*sub_dollar == 0) {
            sub_comm->back_end++;
            sub_comm->back_end_index = i;
            *check_dollar = false;
        }
        else {
            (*sub_dollar)--;

            if ((*quote_array)!=NULL && (*quote_array)[(*qoute_num) - 1] == (*sub_dollar) + 1)
                *qoute = true;
        }
    }

    if ((*data)[i] == '"' && (*data)[i - 1] != '\\') {
        *qoute = !(*qoute);
        if (!(*qoute)) {
            (*qoute_num)--;

            if ((*qoute_num) == 0) {
                 free(*quote_array);
                 quote_array = NULL;
            }
            else
                *quote_array = (int *)realloc(*quote_array, sizeof(int) * (*qoute_num));
        }
        else {
            (*qoute_num)++;

            if ((*qoute_num) == 1)
                *quote_array = (int *)malloc(sizeof(int) * (*qoute_num));
            else
                *quote_array = (int *)realloc(*quote_array, sizeof(int) * (*qoute_num));

            if ((*check_dollar) == true)
                (*quote_array)[(*qoute_num) - 1] = (*sub_dollar) + 1;
            else
                (*quote_array)[(*qoute_num) - 1] = 0;
        }
    }

    if ((*data)[i] == '\'') {
        if (*qoute)
            *qoute = !(*qoute);
        else if ((*data)[i-1] != '\\')
            *qoute = !(*qoute);
        
        if (!(*qoute)){
            (*qoute_num)--;

            if ((*qoute_num) == 0){
                 free(*quote_array);
                 quote_array = NULL;
            }
            else
                *quote_array = (int *)realloc(*quote_array, sizeof(int) * (*qoute_num));
        }
        else{
            (*qoute_num)++;

            if ((*qoute_num) == 1)
                *quote_array = (int *)malloc(sizeof(int) * (*qoute_num));
            else
                *quote_array = (int *)realloc(*quote_array, sizeof(int) * (*qoute_num));

            if ((*check_dollar) == true)
                (*quote_array)[(*qoute_num) - 1] = (*sub_dollar) + 1;
            else
                (*quote_array)[(*qoute_num) - 1] = 0;
        }
    }
}

int mx_check_sub_dollar_comm(char **data, t_com_sub *c, t_ush *ush, int i, bool *check_dollar, int *sub_dollar, bool *qoute, int *qoute_num, int **qoute_arr) {
    mx_find_sub_comm(c, data, i, check_dollar, sub_dollar, qoute, qoute_num, qoute_arr);

    // if index first $ < index last )  
    if (c->back_first_index < c->back_end_index && !(*check_dollar)) {
        bool check_extra_sub_comm = false;
        // check on nested command
        for (int i = c->back_first_index + 2; i < c->back_end_index; i++){
            if ((*data)[i] == '$' && (*data)[i - 1] != '\\' && (*data)[i + 1] == '(')
                check_extra_sub_comm = true;
            if ((*data)[i] == '`' && (*data)[i - 1] != '\\')
                check_extra_sub_comm = true;
            if (check_extra_sub_comm)
                break;
        }
        
        int len = c->back_end_index - c->back_first_index - 2;
        char *inside_comm = strndup(&(*data)[c->back_first_index + 2], len);

        if (check_extra_sub_comm){
            len = c->back_end_index - c->back_first_index - 2;
            char *inside_sub_comm = strndup(&((*data)[c->back_first_index + 2] ), len);
            mx_check_sub_comm(&inside_comm, ush);
            *data = mx_replace_substr(*data, inside_sub_comm, inside_comm);
            char* parsed_inside_comm = mx_strjoin(mx_strjoin("$(",inside_comm), ")");

            c->back_first_index = 0;
            c->back_end_index = mx_strlen(parsed_inside_comm) - 1;
            
            mx_insert_sub_command(c, &parsed_inside_comm, ush, "$");
        }
        else
            mx_insert_sub_command(c, &inside_comm, ush, "$");

        mx_check_lspace(&c->cout_execute);

        if (c->cout_execute == NULL)
            *data = mx_replace_substr(*data, c->temp_str, "");
        else
            *data = mx_replace_substr(*data, c->temp_str, c->cout_execute);
        
        mx_free_Scommand(c);
        if (c->temp_data != NULL) { 
            mx_strdel(data);
            *data = c->temp_data;
        }
        if(*data != NULL)
            return 1;
    }
    
    return 0;
}
