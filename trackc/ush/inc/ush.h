#ifndef USH_H
#define USH_H

#include "../libmx/inc/libmx.h"
#include <pwd.h>
#include <curses.h>
#include <term.h>
#include <limits.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/stat.h>
#include <dirent.h>
#include <termcap.h>
#include <time.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define MX_ENTER 13
#define MX_BACKSPACE 127
#define MX_TAB 9

#define MX_HOME() (mx_getenv("HOME"))
#define MX_PATH() (mx_getenv("PATH"))
#define MX_SHLVL() (mx_getenv("SHLVL"))
#define MX_PWD() (mx_getenv("PWD"))
#define MX_OLDPWD() (mx_getenv("OLDPWD"))

typedef struct s_input {
    int ctrl_c;
    unsigned char input_ch;
    char *input_ch_arr;
    int len;
    int term_width;
    int coursor_position;
    char *command;
    struct termios savetty;
} t_input;

typedef struct  s_env {
    int flag;
    char *comm;
    char *comm_args;
    char **env_var;
} t_env;


typedef struct s_pid {
    int num;
    int index;
    char *str;
    struct s_pid *next;
    struct s_pid *prev;
} t_pid;

 
typedef struct s_ush {
    char *command;
    char *ush_path;
    int return_value;
    int exit_status;
    t_pid *pids;
    char *str_input;
    int exit_non_term;
    pid_t parent_pid;
    int curr_pid;
    char *pwd_l;
    char *pwd;
} t_ush;

typedef struct s_com_sub {
    int back_first;
    int back_first_index;
    int back_end;
    int back_end_index;
    int space;
    int space_first_index;
    int space_end_index;
    char *temp_str;
    char *temp_data;
    char *cout_execute;
    char *temp_join;
    int status;
} t_com_sub;

typedef struct s_redirect {
    int fd_return[2];
    int fd_stdout[2];
    int fd_stderr[2];
    int flag;
    char *_stdout;
    char *_stderr;
} t_redirect;

typedef struct s_queue {
    char *data;
    char operator;
    struct s_queue *next;
} t_queue;

/* ushell */
t_ush* mx_init_ush(char **argv);
void mx_init_shell_env(void);
void mx_free_ush(t_ush *ush);

/* commands */
int mx_cd(char **args, t_ush *ush);
void mx_ush_setenv(char *data, t_ush *ush);

int mx_echo(char **args, char *temp);
char *mx_parse_echo(char **args, int *flag_n, char* temp);
char *mx_fill_parsed_str(char *str, int *flag_n, int flag);
int mx_quote_err(char **parsed_str, int *flag_n, int fg_quote);
void mx_skip_chars(int i, char *str, char *parsed_str, int index);

int mx_env(char **args, t_ush *ush);
void mx_env_err(t_env *env, char **args, int i);
void mx_free_env(t_env *env);
int mx_execute_env_flags(t_env *env, char **args, int i, int *env_index);
t_env *mx_parse_env_args(char **args, t_ush *ush);

int mx_exit(char **input, int *exit_status);

int mx_export(char **args);
int mx_unset(char **args);
int mx_file_exist(char *path);
int mx_find_flag(char *flags, char *arg);
char *mx_getenv(char *var);

int mx_create_path(char *path, t_ush *ush, int flag);
int mx_symlink_check(char **arg, int flag, int link);

int mx_pwd(char **args, t_ush *ush);
int mx_which(char **input);

/* exec str */
char **mx_check_extension(char **str, int res_val);
void mx_check_tilde(char **input);
void mx_exec_child(int *res, char **input, t_redirect *red, t_ush *ush);
int mx_exec_str(t_queue **queue, t_ush *ush);
int mx_exec(t_ush *ush, char *str_input, int flag_redir, char **str_red);
char **mx_echo_split(char *src, char delim);
void mx_exec_child(int *res, char **input, t_redirect *red, t_ush *ush);

/* parse str */
t_queue **mx_parse_command(char *command);
t_queue** mx_parse_str(t_ush *ush);

t_queue *mx_create_queue(char *data, char operation);
void mx_queue_insert(char *array, t_queue **arr_queue);
int mx_count_queue_operation(const char *array);
void mx_queue_pop_front(t_queue **queue);
void mx_queue_push_back(t_queue **queue, char *data, char operation);

/* read str */
char *mx_read_str(t_ush *ush);

char *mx_add_ascii(t_input *input, t_ush *ush);
void mx_set_non_basic(struct termios *savetty);
char *mx_fill_ch_to_comm(t_input *input);
void mx_insert_char(t_input *input, char sym, int index);

void mx_delete_char(t_input *input, int index);
void mx_clear_str();
void mx_free_step(t_input *input, char *temp);

/* pids */
void mx_push_pid(pid_t pid, t_ush **ush);
void mx_swap_pids(t_ush *ush);
void free_pids(t_pid *pids);

/* readirect */
void mx_read_from_pipe(char *str, int len, int *fd);
void mx_write_to_pipe(char *str, int *fd);

t_redirect *mx_init_ancestor(int flag_redir);
void mx_parent_ancestor(t_redirect *redirect, int *return1);
void mx_child_ancestor(t_redirect *redirect);
void mx_free_execute(t_redirect *redirect, char **input);

/* sub command */
int mx_check_builtin_command(char *command);
t_com_sub* mx_init_sub_command();
void mx_check_sub_comm(char **data, t_ush *ush);
char *mx_command_in_path(char *command, char *str_path);
void mx_insert_sub_command(t_com_sub *c, char **data, t_ush *ush, char *symbol);

int mx_check_grave_sub_comm(char **data, t_com_sub *c, t_ush *ush, int i, bool * check_grave, int *sub_dollar, bool * qoute, int * qoute_num, int **quote_array);
int mx_check_sub_dollar_comm(char **data, t_com_sub *c, t_ush *ush, int i, bool *check_dollar, int *sub_dollar, bool *qoute, int *qoute_num, int **qoute_arr);

void mx_free_Scommand(t_com_sub *sub_comm);

/* utility functions */
void mx_check_lspace(char **str);
char *mx_to_replace_operator(char *s);
char **mx_to_split_by_space(char *s, char delim);
char *mx_strcopyn(char *dst, const char *src, int start, int finish);
char *mx_strdupn(const char *data, int start, int finish);
char **mx_strsplit_custom(const char *s, char *c);

#endif
