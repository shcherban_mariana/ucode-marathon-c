#include "../inc/libmx.h"

int mx_count_words(const char *str, char c) {
    int word = 0;

    if (str == NULL) {
        return -1;
    }
    if (str[0] != '\0' && str[0] != c) {
        word++;
    }
    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] == c &&
            str[i + 1] != c &&
            str[i + 1] != '\0') {
            word++;
        }
    }
    return word;
}
