#include "../inc/libmx.h"

char **mx_strsplit(const char *s, char c) {
    if (!s || !*s || !c)
        return NULL;
    int count;
    int i = 0;
    int j = 0;
    count = mx_count_words(s, c);
    char **result = (char **)malloc((count + 1) * sizeof(char *));
    while (*s) {
        if (*s == c) {
            while (*s == c && *s != '\0')
                s++;
        }
        if (*s != c && *s != '\0') {
            char *p = (char *)s;
            while (*s != c && *s != '\0') {
                j++;
                s++;
            }
            result[i] = mx_strndup(p, j);
            i++;
            j = 0;
        }
    }
    result[i] = NULL;
    return result;
}
