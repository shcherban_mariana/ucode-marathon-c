#include "../inc/libmx.h"

int mx_arr_size(char **arr) {
    int counter = 0;
    
    if (arr != NULL){
        while (arr[counter])
            counter++;
        return counter;
    }
    return 0;
}
