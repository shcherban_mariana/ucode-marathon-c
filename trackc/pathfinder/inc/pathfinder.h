#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "libmx.h"

typedef struct s_parse {
    int island_count;
    int array_count;
    char **array_parse;
    char **array_parse1;
    int bridge_count;
    char **bridge_array;
    int **dst_matrix;
    int **floyd_matrix;
} t_parse;

typedef struct s_pathback {
    int pointer;
    int *path;
    int size;
} t_pathback;

//VALIDATION
void file_errors(int argc, char *argv[]);
void isFrirstLineValid(char *str);
void dupl_error(t_parse *info_struct);
void printerr_line(int i);
void duplBridges(char **file, t_parse *info_struct);

//FLOYD
void mx_floyd(t_parse *info_struct);
void mx_all_pathfinding(t_parse *info_struct, int first, int last);
void mx_pathback(t_parse *info_struct, t_pathback *st);
void mx_output(t_parse *info_struct, t_pathback *st);
void mx_print_int(int n);

//PARSER
void mx_parser(char *argv[], t_parse *info_struct);
char **mx_parsestr(char *str, int i);
void mx_create_dstmatrix(t_parse *info_struct);
void mx_create_floydmatrix(t_parse *info_struct);

#endif
