#include "../inc/libmx.h"

void mx_printerr(const char *s) {
    int c = 0;
    char i = s[0];
    while (i != '\0') {
        c++;
        i = s[c];
    }
    write(2, s, c);
}
