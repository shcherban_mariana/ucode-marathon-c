#include "../inc/libmx.h"

char *mx_strstr(const char *s1, const char *s2){
    const char *ptr = s1;
    int len = mx_strlen(s2);
    for(; (ptr = mx_strchr(ptr, *s2)) != 0; ptr++)
        if(mx_strncmp(ptr, s2, len) == 0)
            return (char *)ptr;
    return 0;
}
