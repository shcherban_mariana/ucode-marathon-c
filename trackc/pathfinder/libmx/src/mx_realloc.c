#include "../inc/libmx.h"

void *mx_realloc(void *ptr, size_t size) {
    char *dst = NULL;
    char *ch = ptr;
    if (size == 0) {
        free(ptr);
        ptr = NULL;
        return ptr;
    }
    if (ptr == NULL)
        dst = malloc(size * sizeof(ptr));
    else {
        dst = malloc(size * sizeof(ptr));
        if (dst == NULL)
            return NULL;
        size_t i = 0;
        for (i = 0; ch[i] && i < size; i++)
            dst[i] = ch[i];
    }
    return dst;
}
