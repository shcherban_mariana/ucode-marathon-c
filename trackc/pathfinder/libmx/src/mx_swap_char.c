#include "../inc/libmx.h"

void mx_swap_char(char *s1, char *s2) {
    char buff1 = *s1;
    char buff2 = *s2;

    *s1 = buff2;
    *s2 = buff1;
}
