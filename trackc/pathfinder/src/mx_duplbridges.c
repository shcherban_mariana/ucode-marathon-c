#include "../inc/pathfinder.h"

static int count_array(char **arr) {
    int i = 1;
    while (arr[i] != NULL)
        i++;
    return (i - 1);
}

static void add_inf(t_parse *info_struct, char *buff) {
    info_struct->array_parse1[info_struct->array_count] = mx_strdup(buff);
    info_struct->array_count += 1;
}

static void add_inf1(t_parse *info_struct, char *buff) {
    info_struct->bridge_array[info_struct->array_count] = mx_strdup(buff);
    info_struct->array_count += 1;
}

void duplBridges(char **file, t_parse *info_struct) {
    
    info_struct->bridge_count = count_array(file);
    info_struct->array_count = 0;

    info_struct->array_parse1 = (char **)malloc(((info_struct->bridge_count * 3) + 1) * sizeof(char *));

    for (int i = 0; i < ((info_struct->bridge_count * 3) + 1); i++) {
        info_struct->array_parse1[i] = NULL;
    }
    
    char **buff;
    char **buff1;

    for (int i = 1; file[i]; i++) {
        buff = mx_parsestr(file[i], i);
        add_inf(info_struct, buff[0]);
        add_inf(info_struct, buff[1]);
        add_inf(info_struct, buff[2]);
        mx_del_strarr(&buff);
    }

    info_struct->bridge_array = (char **)malloc(((info_struct->bridge_count * 2) + 1) * sizeof(char *));
    info_struct->array_count = 0;

    for (int j = 0; j < ((info_struct->bridge_count * 2) + 1); j++) {
        info_struct->bridge_array[j] = NULL;
    }

    for (int k = 1; file[k]; k++) {
        buff1 = mx_parsestr(file[k], k);
        add_inf1(info_struct, buff1[0]);
        add_inf1(info_struct, buff1[1]);
        mx_del_strarr(&buff1);
    }
    dupl_error(info_struct);
}
