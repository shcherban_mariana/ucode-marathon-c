#include "../inc/pathfinder.h"

static void print_error(t_parse *info_struct, int i) {
    for (int j = i + 2; j < ((info_struct->bridge_count) * 2) && info_struct->bridge_array[j] != NULL && info_struct->bridge_array[j + 2] != NULL; j++) {
        if (i % 2 != 0)
            i++;
        if (j % 2 != 0 || j == i)
            j++;
        if ((mx_strcmp(info_struct->bridge_array[i], info_struct->bridge_array[j]) == 0) &&
            (mx_strcmp(info_struct->bridge_array[i + 1], info_struct->bridge_array[j + 1]) == 0)) {
            mx_printerr("error: duplicate bridges\n");
            mx_del_strarr(&info_struct->array_parse1);
            mx_del_strarr(&info_struct->array_parse);
            exit(1);
        }
        if ((mx_strcmp(info_struct->bridge_array[i], info_struct->bridge_array[j + 1]) == 0) &&
            (mx_strcmp(info_struct->bridge_array[i + 1], info_struct->bridge_array[j]) == 0)) {
            mx_printerr("error: duplicate bridges\n");
            mx_del_strarr(&info_struct->array_parse1);
            mx_del_strarr(&info_struct->array_parse);
            exit(1);
        }
    }
}

void printerr_line(int i) {
    mx_printerr("error: line ");
    mx_printerr(mx_itoa(i + 1));
    mx_printerr(" is not valid\n");
    exit(1);
}

void dupl_error(t_parse *info_struct) {
    int i;
    for (i = 0; info_struct->bridge_array[i] && info_struct->bridge_array[i + 2] != NULL && info_struct->bridge_array[i] != NULL; i++) {
        print_error(info_struct, i);
    }
}

void file_errors(int argc, char *argv[]) {
    if (argc != 2) {
        mx_printerr("usage: ./pathfinder [filename]\n");
        exit(1);
    }
    char buff;
    int arg = open(argv[1], O_RDONLY);
    int arg1 = read(arg, &buff, 1);

    close(arg);

    if (arg1 <= 0) {
        mx_printerr("error: file ");
        mx_printerr(argv[1]);
        mx_printerr(" is empty\n");
        exit(1);
    }
}

void isFrirstLineValid(char *str) {
    for (int i = 0; i < mx_strlen(str); i++) {
        if (!mx_isdigit(str[i])) {
            mx_printerr("error: line 1 is not valid\n");
            exit(1);
        }
    }
}
