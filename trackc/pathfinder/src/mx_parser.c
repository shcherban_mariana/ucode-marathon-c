#include "../inc/pathfinder.h"

static int getIndex(t_parse *info_struct, char *c) {
    for (int i = 0; info_struct->array_parse[i]; i++) {
        if (mx_strcmp(info_struct->array_parse[i], c) == 0)
            return i;
    }
    return -1;
}

static void mx_writetomatrix(t_parse *info_struct, char **buff) {
    int dst = mx_atoi(buff[2]);
    int count = info_struct->island_count;

    info_struct->dst_matrix[getIndex(info_struct, buff[0])][getIndex(info_struct, buff[1])] = dst;
    info_struct->dst_matrix[getIndex(info_struct, buff[1])][getIndex(info_struct, buff[0])] = dst;

    for (int i = 0; i < count; i++) {
        for (int j = 0; j < count; j++) {
            if ((i != j) && (info_struct->dst_matrix[i][j] == 0)) {
                int value = 214748;
                info_struct->dst_matrix[i][j] = value;
            }
        }
    }
}

static void mx_add_toarr(t_parse *info_struct, char *temp) {
    int value = 0;

    for (int j = 0; info_struct->array_parse[j] && j < info_struct->array_count; j++) {
        if (mx_strcmp(info_struct->array_parse[j], temp) == 0)
            value = -1;
    }
    if (value == 0) {
        if (info_struct->array_count >= info_struct->island_count) {
            mx_printerr("error: invalid number of islands\n");
            exit(1);
        }
        info_struct->array_parse[info_struct->array_count] = mx_strdup(temp);
        info_struct->array_count += 1;
    }
}

static void mx_fillstruct(t_parse *info_struct, char **str) {
    char **buff;
    info_struct->array_count = 0;

    for (int i = 0; i < info_struct->island_count + 1; i++) {
        info_struct->array_parse[i] = NULL;
    }

    for (int j = 1; str[j]; j++) {
        buff = mx_parsestr(str[j], j);

        mx_add_toarr(info_struct, buff[0]);
        mx_add_toarr(info_struct, buff[1]);
        mx_writetomatrix(info_struct, buff);
        mx_del_strarr(&buff);
    }
}

void mx_parser(char *argv[], t_parse *info_struct) {
    char *file = mx_file_to_str(argv[1]);
    char **str = mx_strsplit(file, '\n');

    isFrirstLineValid(str[0]);
    info_struct->island_count = mx_atoi(str[0]);
    mx_create_dstmatrix(info_struct);

    info_struct->array_parse = malloc((info_struct->island_count + 1) * sizeof(char *));
    info_struct->array_count = 0;
    info_struct->bridge_count = 0;
    mx_fillstruct(info_struct, str);
    if (info_struct->array_count != info_struct->island_count) {
        mx_printerr("error: invalid number of islands\n");
        mx_strdel(&file);
        mx_del_strarr(&str);
        mx_del_strarr(&info_struct->array_parse);
        exit(1);
    }
    mx_create_floydmatrix(info_struct);
    duplBridges(str, info_struct);
    mx_strdel(&file);
    mx_del_strarr(&str);
}
