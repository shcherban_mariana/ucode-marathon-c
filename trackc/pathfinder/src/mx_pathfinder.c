#include "../inc/pathfinder.h"

static int getIndex(t_pathback *st) {
    return st->path[st->size];
}

static void push_to_stack(t_pathback *st, int value) {
    if (st->size < st->pointer) {
        st->size++;
        st->path[st->size] = value;
    }
}

static int pop_from_stack(t_pathback *st) {
    if (st->size > 1) {
        st->size--;
        return st->path[st->size + 1];
    }
    return st->path[1];
}

static bool check_next_parse(t_parse *info_struct, t_pathback *st, int next_parse) {
    int k = getIndex(st);
    int j = st->path[0];

    if (k != next_parse) {
        if (info_struct->floyd_matrix[k][next_parse] == info_struct->dst_matrix[j][k] - info_struct->dst_matrix[j][next_parse])
            return true;
    }
    return false;
}

static void st_init(t_parse *info_struct, t_pathback **st, int i, int j) {
    *st = malloc(sizeof(t_pathback));

    int buff = info_struct->island_count;

    if ((*st) == NULL)
        exit(1);

    (*st)->path = malloc(sizeof(int) * buff + 1);
    (*st)->size = 1;
    (*st)->path[1] = i;
    (*st)->path[0] = j;
    (*st)->pointer = buff;
}

void mx_pathback(t_parse *info_struct, t_pathback *st) {
    int k = st->pointer;

    if (getIndex(st) == st->path[0]) {
        mx_output(info_struct, st);
        return;
    }
    else {
        for (int i = 0; i < k; i++) {
            if (check_next_parse(info_struct, st, i)) {
                push_to_stack(st, i);
                mx_pathback(info_struct, st);
                pop_from_stack(st);
            }
        }
    }
}

void mx_all_pathfinding(t_parse *info_struct, int first, int last) {
    t_pathback *st = NULL;

    st_init(info_struct, &st, first, last);
    mx_pathback(info_struct, st);
    free(st->path);
    free(st);
}
