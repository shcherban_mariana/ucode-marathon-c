#include "../inc/pathfinder.h"

static bool toCheckName(char *str) {
    for (int i = 0; str[i]; i++) {
        if (!mx_isalpha(str[i]))
            return false;
    }
    if (mx_strlen(str) <= 0)
        return false;
    return true;
}

static bool toCheckDist(char *str) {
    for (int i = 0; str[i]; i++) {
        if (!mx_isdigit(str[i]))
            return false;
    }
    if (mx_strlen(str) <= 0)
        return false;
    return true;
}

static bool maxDist(char *str) {
    int dst = mx_atoi(str);
    int max_int = 2147483647;
    if (dst >= max_int) {
        return false;
    }
    return true;
}

char **mx_parsestr(char *str, int i) {
    int dash = mx_get_char_index(str, '-');
    int comma = mx_get_char_index(str, ',');
    int s = mx_strlen(str);
    int countd = mx_count_ch(str, '-');
    int countc = mx_count_ch(str, ',');

    char **p_str = (char **)malloc(4 * sizeof(char *));

    if (dash < 0 || comma < 0 || s < 5 || countc > 1 || countd > 1)
        printerr_line(i);

    p_str[0] = mx_strndup(str, dash);
    p_str[1] = mx_strndup((str + dash + 1), (comma - dash - 1));
    p_str[2] = mx_strndup((str + comma + 1), (s - comma));
    p_str[3] = NULL;

    if (!toCheckName(p_str[0]) || !toCheckName(p_str[1]) || !toCheckDist(p_str[2]) || mx_strcmp(p_str[0], p_str[1]) == 0 || ((mx_strcmp(p_str[0], p_str[1]) != 0) && mx_atoi(p_str[2]) == 0)) {
        mx_del_strarr(&p_str);
        printerr_line(i);
    }
    if (!maxDist(p_str[2])) {
        mx_printerr("error: sum of bridges lengths is too big\n");
        exit(1);
    }
    else
        return p_str;
    mx_del_strarr(&p_str);
}
