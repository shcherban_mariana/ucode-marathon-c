#include "../inc/pathfinder.h"

int main(int argc, char *argv[]) {
    t_parse *info_struct = malloc(sizeof(t_parse));
    info_struct->island_count = 0;

    int count;

    file_errors(argc, argv);
    mx_parser(argv, info_struct);
    count = info_struct->island_count;
    mx_floyd(info_struct);
    for (int i = 0; i < count; i++) {
        for (int j = 0; j < count; j++) {
            if (i < j) {
                mx_all_pathfinding(info_struct, i, j);
            }
        }
    }
    return 0;
}
