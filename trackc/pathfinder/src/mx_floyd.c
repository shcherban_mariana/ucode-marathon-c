#include "../inc/pathfinder.h"

void mx_floyd(t_parse *info_struct) {
    int temp = info_struct->island_count;
    int new_dst;

    for (int i = 0; i < temp; i++) {
        for (int j = 0; j < temp; j++) {
            for (int k = 0; k < temp; k++) {
                new_dst = info_struct->dst_matrix[j][i] + info_struct->dst_matrix[i][k];
                if (new_dst < info_struct->dst_matrix[j][k])
                    info_struct->dst_matrix[j][k] = new_dst;
            }
        }
    }
}
