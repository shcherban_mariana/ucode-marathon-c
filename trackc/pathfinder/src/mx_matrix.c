#include "../inc/pathfinder.h"

void mx_create_dstmatrix(t_parse *info_struct) {
    int value = info_struct->island_count;

    info_struct->dst_matrix = NULL;
    info_struct->dst_matrix = (int **)malloc((value) * sizeof(int *) * value);

    for (int i = 0; i < value; i++) {
        info_struct->dst_matrix[i] = (int *)malloc(sizeof(int) * (value));
        for (int j = 0; j < value; j++) {
            info_struct->dst_matrix[i][j] = 0;
        }
    }
}

void mx_create_floydmatrix(t_parse *info_struct) {
    int value = info_struct->island_count;

    info_struct->floyd_matrix = NULL;
    info_struct->floyd_matrix = (int **)malloc((value) * sizeof(int *) * value);

    for (int i = 0; i < value; i++) {
        info_struct->floyd_matrix[i] = (int *)malloc(sizeof(int) * (value));
        for (int j = 0; j < value; j++) {
            info_struct->floyd_matrix[i][j] = info_struct->dst_matrix[i][j];
        }
    }
}
