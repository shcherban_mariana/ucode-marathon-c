#include "../inc/pathfinder.h"

static void mx_print_dist(t_parse *info_struct, t_pathback *st) {
    int res_sum = 0;
    int value = st->size;

    mx_printstr("\nDistance: ");
    if (value == 2)
        mx_printint(info_struct->dst_matrix[st->path[value]][st->path[value - 1]]);
    else {
        for (int i = 1; i < value; i++) {
            mx_printint(info_struct->dst_matrix[st->path[i]][st->path[i + 1]]);

            res_sum += info_struct->dst_matrix[st->path[i]][st->path[i + 1]];
            
            if (i + 1 < value) {
                mx_printstr(" + ");
            }
            else {
                mx_printstr("");
            }
        }
        mx_printstr(" = ");
        mx_printint(res_sum);
    }
}

void mx_output(t_parse *info_struct, t_pathback *st) {
    int j = st->path[1];
    int i = st->path[0];
    int value = st->size;

    mx_printstr("========================================");
    mx_printstr("\n");
    mx_printstr("Path: ");
    mx_printstr(info_struct->array_parse[j]);
    mx_printstr(" -> ");
    mx_printstr(info_struct->array_parse[i]);
    mx_printstr("\nRoute: ");

    for (int i = 1; i <= value; i++) {
        mx_printstr(info_struct->array_parse[st->path[i]]);
        if (i < value) {
            mx_printstr(" -> ");
        } 
        else {
            mx_printstr("");
        }
    }
    
    mx_print_dist(info_struct, st);
    mx_printstr("\n");
    mx_printstr("========================================\n");
}
